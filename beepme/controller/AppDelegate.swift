//
//  AppDelegate.swift
//  beepme
//
//  Created by Macbook on 19/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let notificationCenter = UNUserNotificationCenter.current()
    
    //Firebase
    let gcmMessageIDKey = "gcm.message_id"


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //firebase
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        UNUserNotificationCenter.current().delegate = self
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        
        //Solicit permission from user to receive notifications
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        
        //get application instance ID
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                
                let defaults = UserDefaults.standard
                defaults.set(result.token, forKey: "fcmToken")
                defaults.synchronize ()
                
                
            }
        }
        
        application.registerForRemoteNotifications()
        
        
        
        
        //цвет строки состояния
        var statusBarUIView: UIView? {
          if #available(iOS 13.0, *) {
              let tag = 38482
              let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

              if let statusBar = keyWindow?.viewWithTag(tag) {
                  return statusBar
              } else {
                  guard let statusBarFrame = keyWindow?.windowScene?.statusBarManager?.statusBarFrame else { return nil }
                  let statusBarView = UIView(frame: statusBarFrame)
                  statusBarView.tag = tag
                  keyWindow?.addSubview(statusBarView)
                  return statusBarView
              }
          } else if responds(to: Selector(("statusBar"))) {
              return value(forKey: "statusBar") as? UIView
          } else {
              return nil
          }
        }
        
        statusBarUIView?.backgroundColor = hexStringToUIColor(hex: "165d73")
        
        //UIApplication.shared.statusBarView?.backgroundColor = hexStringToUIColor(hex: "165d73")
        
        //чтобы таймер работал после закрытия приложения, нужен для уведомления
        //application.beginBackgroundTask(withName: "showNotification", expirationHandler: nil)

        //фоновое обновление данных, а именно получение информации о сообщениях и прочем
        //UIApplication .shared.setMinimumBackgroundFetchInterval(10)
            
        //UIApplication .shared.setMinimumBackgroundFetchInterval (UIApplication .backgroundFetchIntervalMinimum)

        //registerForPushNotifications()
        

        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        let slides_was_showed = defaults.bool(forKey: "slides_was_showed")
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if slides_was_showed == false || slides_was_showed == nil {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let altController = storyboard.instantiateViewController(withIdentifier: "slidesController")
            defaults.set(true, forKey: "slides_was_showed")
            defaults.synchronize ()

            self.window?.rootViewController = altController
            self.window?.makeKeyAndVisible()
            
            return true
        } else {
            if token != nil {
                let mainController = storyboard.instantiateViewController(withIdentifier: "mainView")
                self.window?.rootViewController = mainController
            } else {
                let mainController = storyboard.instantiateViewController(withIdentifier: "authControler")
                self.window?.rootViewController = mainController
            }
            return true
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
    }
    
    
    
    
    // Функция для фоновой выборки
    func application(
        _ application: UIApplication,
        performFetchWithCompletionHandler
        completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        //1
        let fetchViewController = ViewController()
        fetchViewController.getDataFromServer({})
        completionHandler(.newData)
        
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        //снимаем цифру со значка приложения
        UIApplication.shared.applicationIconBadgeNumber = 0

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func scheduleNotification(notificationType: String) {
        
        let content = UNMutableNotificationContent() // Содержимое уведомления
        
        content.title = notificationType
        content.body = "This is example how to create "
        content.sound = UNNotificationSound.default
        content.badge = 1
    }
    
    // УВЕДОМЛЕНИЯ
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        
        
        let token = tokenParts.joined()
        print("Device Token: \(token)")
        //получили токен для уведомлений
        // записываем его в файл и сохраняем. Позже отправим на сервер
        
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }

    
}

//цвет строки состояния
extension UIApplication {
    /*
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }*/
    
}

//скрыть клавиатуру при нажатии на свободное место
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UITextField {
    func setBottomBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
    
    func setNoBorder() {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.white.cgColor
    }
    
    
    
}


//firebase

extension AppDelegate: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
    
}

extension AppDelegate: MessagingDelegate{
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let defaults = UserDefaults.standard
        defaults.set(fcmToken, forKey: "fcmToken")
        defaults.synchronize ()
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}

extension UIView{
    func stylyzateCarNumberView() {
        self.layer.cornerRadius = 5.0
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 2.0
    }
}

