//
//  oneConnectViewController.swift
//  beepme
//
//  Created by Macbook on 21/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class oneConnectViewController: UIViewController, UITableViewDelegate, UITableViewDataSource , UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
    
    
    var locationManager: CLLocationManager!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    @IBOutlet weak var viewForCollectionView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var constraintViewForStickers: NSLayoutConstraint!
    
    @IBOutlet weak var myNavigationItem: UINavigationItem!
    
    
    var timer = Timer()

    
    var my_id: Int?

    
    var numberOfItemsPerRow = 4
    
    var id: Int?
    var noBackButton = false
    var hasMapCell = false //если есть координаты то становится true и добавляет в таблицу на 1 ячейку больше, для карты
    
    var connect: Connect?
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    
    @IBOutlet weak var completeChatButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        //self.tableView.rowHeight = 190
        self.tableView.rowHeight = UITableView.automaticDimension

        collectionView.delegate = self
        collectionView.dataSource = self
        
        let defaults = UserDefaults.standard
        my_id = defaults.integer(forKey: "my_id")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("hasMapCell=\(hasMapCell)")
        getConnect()
        
        if self.connect != nil {
            if self.connect?.title != nil {
                self.myNavigationItem.title = self.connect?.title!
            } else {
                self.myNavigationItem.title = "Коннект без названия"
            }
        }
        //print(self.myNavigationItem.title)
        LoadingIndicator.shared.startAnimation()

        
        
        timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getMesseges), userInfo: nil, repeats: true)
        
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[1]
            tabItem.badgeValue = nil
        }
        
        
    }
    
    func getConnect(){
        let connectsApiService = ConnectsApiService()
        connectsApiService.get_connect(id: id!) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()

            self.connect = result
            self.tableView.reloadData()
            
            //заголовок
            if result?.title != nil {
                self.navigationItem.title = result?.title!
            }
            
            //стикеры для отправки
            self.updateStickersForSending()
            
            //кнопка назад
            if self.connect?.stickers_for_sending != nil {
                if (self.connect!.stickers_for_sending!.contains(10)) {
                    self.navigationItem.backBarButtonItem?.isEnabled = true
                } else {
                    self.navigationItem.backBarButtonItem?.isEnabled = false
                }
            } else {
                self.navigationItem.backBarButtonItem?.isEnabled = false
            }
            
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func updateStickersForSending(){
        if connect!.stickers_for_sending!.count == 0 || connect?.stickers_for_sending == [11] {
            print("скрываем")
            //viewForCollectionView.frame.size.height = 0
            //viewForCollectionView.frame = CGRect(x:0, y: 0, width:0, height:0)
            viewForCollectionView.isHidden = true
            //collectionView.isHidden = true
            
            let constraint = tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            NSLayoutConstraint.activate([constraint])

            
            //tableView.frame.size.height += 116
        } else {
            print("показываем")
            viewForCollectionView.frame.size.height = 116

            collectionView.reloadData()
            viewForCollectionView.isHidden = false
            let constraint = tableView.bottomAnchor.constraint(equalTo: viewForCollectionView.topAnchor)
            NSLayoutConstraint.activate([constraint])
        }
    }
    
    @objc func getMesseges(){
        
        /*
        var responseString = "null"
        
        let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path)
        
        let needEthernet = false
        let url = URL(string: dict!.object(forKey: "url") as! String )!
        var request2 = URLRequest(url: url)
        request2.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request2.httpMethod = "POST"
        let postString2 = "k=\(dict!.object(forKey: "k")!)&command=get_one_connect&phone=\(userData.phone!)&sessionKey=\(userData.sessionKey!)&connect_id=\(connectId!)"
        print("postString=\(postString2)")
        
        request2.httpBody = postString2.data(using: .utf8)
        
        let task2 = URLSession.shared.dataTask(with: request2) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error ?? "null" as! Error)")
                if needEthernet==true {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Internet connection required", message: "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Oh, something went wrong", message: "We received error information and will fix it soon.", preferredStyle: UIAlertController.Style.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        (UIAlertAction) -> Void in
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            responseString = String(data: data, encoding: .utf8) ?? "null"
            print(responseString)
            
            if  responseString != "" {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    do {
                        let decoder = JSONDecoder()
                        let decodedJSON = try? decoder.decode(messages.self, from: data)
                        dump(decodedJSON)
                        
                        if decodedJSON?.success == true {
                            self.response = decodedJSON
                            
                            if decodedJSON?.my_position == "reciever" {
                                if decodedJSON?.author_car_number != nil {
                                    self.navigationItem.title = decodedJSON?.author_car_number!
                                }
                                if decodedJSON?.author_car_color != nil {
                                    let colorConverter = hexStringtoUIColor()
                                    
                                    self.colorRect.tintColor = colorConverter.hexStringToUIColor(hex: (decodedJSON?.author_car_color!)!)
                                }
                            } else {
                                self.navigationItem.title = decodedJSON?.reciever_car_number!
                                if decodedJSON?.reciever_car_color != nil {
                                    let colorConverter = hexStringtoUIColor()
                                    
                                    self.colorRect.tintColor = colorConverter.hexStringToUIColor(hex: (decodedJSON?.reciever_car_color!)!)
                                }
                            }
                            
                            
                            
                            if decodedJSON?.messages != nil  {
                                
                                let newMesseges = decodedJSON?.messages!
                                self.updateStickersForSending()
                                
                                if self.messages != nil {
                                    if newMesseges! != self.messages! {
                                        if decodedJSON?.messages?.count == 0 {
                                            //показываем картинку что нет коннектов
                                            
                                        } else {
                                            self.messages = newMesseges
                                            self.tableView.reloadData()
                                        }
                                    }
                                } else {
                                    print("Это")
                                    if decodedJSON?.messages?.count == 0 {
                                        //показываем картинку что нет коннектов
                                        
                                    } else {
                                        self.messages = newMesseges
                                        self.tableView.reloadData()
                                    }
                                }
                                
                            }
                        } else {
                            if decodedJSON?.code == 530 {
                                
                                let defaults = UserDefaults.standard
                                defaults.set("", forKey: "phone")
                                defaults.set("", forKey: "sessionKey")
                            } else {
                                let alert = UIAlertController(title: "Ошибка", message: decodedJSON?.description , preferredStyle: UIAlertController.Style.alert)
                                let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                                    (UIAlertAction) -> Void in
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                                
                                errorLoging.serverLog(errorText: responseString, command: "get_one_connect")
                            }
                        }
                    }
                }
            }
        }
        task2.resume()
 */
    }
    
    @IBAction func backClick(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if connect?.messages != nil {
            
            var count = connect!.messages!.count
            
            if connect?.location_lattitude != nil && connect?.location_longitude != nil  &&  hasMapCell {
                    count += 1
            } else {
                hasMapCell = false
            }
            
            if connect!.stickers_for_sending!.contains(11) {
                count += 1
            }
            return count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var index = indexPath.row
        
        if index >= connect!.messages!.count && connect!.stickers_for_sending!.contains(11) {
            // Кнопка отмены
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "buttonMessegeCell") as! ButtonMessegeCell
            cell.button.addTarget(self, action: #selector(cancelButtonAction), for: .touchUpInside)
            return cell

        }
        
        
        if connect!.messages![index].location_lattitude != nil && connect!.messages![index].location_longitude != nil && connect!.messages![index].type == "map"  {
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "mapCell") as! mapTableViewCell
            
            //регион для карты
            let location = CLLocation(latitude: Double((connect!.messages![index].location_lattitude)!) , longitude: Double((connect!.messages![index].location_longitude)!) )
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            cell.mapView.setRegion(region, animated: true)
            
            //ставим точку
            let CLLCoordType = CLLocationCoordinate2D(latitude: Double((connect!.messages![index].location_lattitude)!) , longitude: Double((connect!.messages![index].location_longitude)!) );
            let anno = MKPointAnnotation();
            anno.coordinate = CLLCoordType;
            cell.mapView.addAnnotation(anno);
            
            
            return cell
        } else if connect!.messages![index].text != nil && connect!.messages![index].type == "text" {
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "textTableViewCell") as! TextTableViewCell
            cell.label.text = connect!.messages![index].text!
            //cell.label.sizeToFit()
            
            return cell

        } else if connect!.messages![index].image_link != nil && connect!.messages![index].type == "image" { // Блок с изображением
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "imageTableViewCell") as! ImageTableViewCell
            cell.myImageView?.downloaded(from: connect!.messages![index].image_link!)
            return cell

        } else if (connect!.messages![index].left_text != nil || connect!.messages![index].right_text != nil) && connect!.messages![index].type == "price"  { // Блок с тарифом
            // Блоки с тарифом
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "priceTableViewCell") as! RateTableViewCell
            cell.restorationIdentifier = "priceCell"
            if connect!.messages![index].left_text != nil {
                cell.leftLabel.text = connect!.messages![index].left_text!
            }
            
            if connect!.messages![index].right_text != nil {
                cell.rightLabel.text = connect!.messages![index].right_text!
            }
            
            return cell

        } else if my_id == connect!.messages![index].author && connect!.messages![index].type == "sticker"  {
            //я отправитель
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "sendedMessegeCell") as! sendedMessegeTableViewCell
            
            //стикер
            if connect!.messages![index].sticker_id != 0 {
                cell.imageVi.image = UIImage(named: "sticker\(connect!.messages![index].sticker_id!)")
            }
            
            //время
            if connect!.messages![index].time != nil {
                cell.timeLabel.text = DateTimeFormatter.getTime(stringDate: connect!.messages![index].time! )
            }
            
            
            if index == connect!.messages!.count - 1 {
                self.scrollToLastRow()
            }
            
            return cell
            
            
        } else if connect!.messages![index].type == "sticker"  {
            
            //я получатель
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "recievedMessegeCell") as! recievedMessegeTableViewCell
            
            //стикер
            if connect!.messages![index].sticker_id != 0 {
                cell.imageVi.image = UIImage(named: "sticker\(connect!.messages![index].sticker_id!)")
            }
            //время
            if connect!.messages![index].time != nil {
                cell.timeLabel.text = DateTimeFormatter.getTime(stringDate: connect!.messages![index].time! )
            }
            
            
            return cell
        } else
        {
            return UITableViewCell()
        }
        
        
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //если нажата карта то открываем в картах
        if indexPath.row == 0 {
            if connect?.location_lattitude != nil  && connect?.location_longitude != nil {
                openMapForPlace(lat: Double((connect?.location_lattitude)!) as! Double, long: Double((connect?.location_longitude)!) as! Double)

            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row >= connect!.messages!.count && connect!.stickers_for_sending!.contains(11) {
            // Кнопка отмены
            return 60
        }
        
        if connect!.messages![indexPath.row].type == "price"{
            return 70
        } else if connect!.messages![indexPath.row].type == "text" {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "textTableViewCell") as! TextTableViewCell
            
            //let labelHeight = cell.label.heightForView()
            
            //cell.label.sizeToFit()
            let labelHeight = view.heightForView(text: connect!.messages![indexPath.row].text!, width: cell.label.bounds.size.width)
            //cell.label.bounds.size.height
            let height = labelHeight + 50
            print("labelHeight ", labelHeight)

            print(height)
            return height
            
        } else {
            return 190
        }
        
    }
    
    func openMapForPlace(lat: Double, long: Double) {
        let regionDistance: CLLocationDistance = 10000000
        let coordinates = CLLocationCoordinate2DMake(lat, long)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        
        MKMapItem.openMaps(with: [], launchOptions: options)
    }
    
    func scrollToLastRow() {
        let indexPath = NSIndexPath(row: connect!.messages!.count - 1, section: 0)
        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if connect?.type == "parking" {
            //self.viewForCollectionView.isHidden = true
            return 0
            
        } else {
            if connect?.stickers_for_sending != nil {
                return connect!.stickers_for_sending!.count
            } else {
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? stickersCollectionViewCell
        let stickerName = "sticker\(connect!.stickers_for_sending![indexPath.row])"
        print(stickerName)
        itemCell!.imageView.image = UIImage(named: stickerName)
            
        if connect!.stickers_for_sending?[indexPath.row] == 4 {
            itemCell?.title.text = "Отмена"
        } else {
            itemCell?.title.text = ""
        }
        
        return itemCell!
    }
    
    //задаем ширину ячеек в collectionView
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: size + 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let stickerID = connect?.stickers_for_sending![indexPath.row]
        
        let connectsApiService = ConnectsApiService()
        connectsApiService.sendSticker(connectID: id! , stickerID: stickerID!) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()
            
            if result!["error"] as! Int? == 0 {
                //self.connect = result!["connect"] as! Connect?
                self.getConnect()
                self.tableView.reloadData()
                self.collectionView.reloadData()
                print("self.connect \(self.connect)")
                
            } else if result!["error"] as! Int? == 1 {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Вы заблокированы", message: "Вы не можете отправлять стикеры данному пользователю", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хорошо", style: .default, handler: nil ))
                    self.present(alert, animated: true, completion: nil)
                }
                
            }
            
            
        }
        
    }
    
    @objc func cancelButtonAction(sender: UIButton!)  {
        LoadingIndicator.shared.startAnimation()
        let vehicleApiService = VehicleApiService()
        vehicleApiService.finishParking(sessionID: connect!.parking_session!.id!) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()
            self.getConnect()
        }
    }
    
    @IBAction func completeChat(_ sender: UIBarButtonItem) {
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
