//
//  connectsViewController.swift
//  beepme
//
//  Created by Macbook on 21/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit

class connectsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var page = 1
    
    var connects: [Connect]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let tabItems = self.tabBarController?.tabBar.items {
            let tabItem = tabItems[1]
            tabItem.badgeValue = nil
        }
        
        if connects == nil {
            LoadingIndicator.shared.startAnimation()
        }
        
        
        let connectsApiService = ConnectsApiService()
        connectsApiService.get_connects(page: page) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()

            self.connects = result
            self.tableView.reloadData()
        }
    }
    
    private func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.tableFooterView = UIView()
        self.tableView.rowHeight = 100
    }
    
    func update(){
        /*
        var responseString = "null"
        activityIndicator.startAnimating()
        self.activityIndicator.isHidden = false
        
        let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path)
        
        let needEthernet = false
        let url = URL(string: dict!.object(forKey: "url") as! String )!
        var request2 = URLRequest(url: url)
        request2.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request2.httpMethod = "POST"
        let postString2 = "k=\(dict!.object(forKey: "k")!)&command=get_connects&phone=\(userData.phone!)&sessionKey=\(userData.sessionKey!)"
        print("postString=\(postString2)")
        
        request2.httpBody = postString2.data(using: .utf8)
        
        let task2 = URLSession.shared.dataTask(with: request2) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error ?? "null" as! Error)")
                if needEthernet==true {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Internet connection required", message: "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Oh, something went wrong", message: "We received error information and will fix it soon.", preferredStyle: UIAlertController.Style.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        (UIAlertAction) -> Void in
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            responseString = String(data: data, encoding: .utf8) ?? "null"
            print(responseString)
            
            if  responseString != "" {
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                    self.activityIndicator.isHidden = true
                    do {
                        let decoder = JSONDecoder()
                        let decodedJSON = try? decoder.decode(connects.self, from: data)
                        dump(decodedJSON)
                        
                        if decodedJSON?.success == true {
                            self.connects = decodedJSON?.connects
                            
                            //если коннектов одна штука то открываем экран диалога
                            if decodedJSON?.connects != nil {
                                if decodedJSON?.connects?.count == 0 {
                                    //показываем картинку что нет коннектов
                                } else if decodedJSON?.connects?.count == 1 {
                                    self.performSegue(withIdentifier: "segueOneConnect", sender: 0)
                                } else {
                                    self.tableView.reloadData()
                                }
                            }
                        } else {
                            if decodedJSON?.code == 530 {
                                
                                let defaults = UserDefaults.standard
                                defaults.set("", forKey: "phone")
                                defaults.set("", forKey: "sessionKey")
                            } else {
                                let alert = UIAlertController(title: "Ошибка", message: decodedJSON?.description , preferredStyle: UIAlertController.Style.alert)
                                let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                                    (UIAlertAction) -> Void in
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
        }
        task2.resume()
 */
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.connects != nil {
            return self.connects!.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")
        if self.connects![indexPath.row].title != nil {
            cell?.textLabel!.text = self.connects![indexPath.row].title
        } else {
            cell?.textLabel!.text = "Коннект без названия"
        }
        cell?.textLabel?.font = UIFont.systemFont(ofSize: 21)
        
        if self.connects![indexPath.row].start_time != nil {
            cell?.detailTextLabel!.text = DateTimeFormatter.getDate(stringDate: self.connects![indexPath.row].start_time!)
        } else {
            cell?.detailTextLabel!.text = ""
        }
        
        

        
        /*
        if let timeResult = (Int(self.connects![indexPath.row].start_time!)) {
            let resultDate = Date(timeIntervalSince1970: TimeInterval(timeResult))
            
            cell?.detailTextLabel!.text = resultDate.getElapsedInterval()
        } else {
            
            cell?.detailTextLabel!.text = NSLocalizedString("less than 2 hours ago", comment: "")
        }
 */
        cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 13)
        
        if self.connects![indexPath.row].type == "person" {
            cell?.imageView?.image = UIImage(named: "person_connect")
        } else if self.connects![indexPath.row].type == "parking" {
            cell?.imageView?.image = UIImage(named: "parking_connect")
        }
        cell?.imageView?.bounds.size.height = (cell?.bounds.height)! - 10
        
        cell!.layer.borderColor = UIColor.white.cgColor
        cell!.layer.borderWidth = 6
        
        
        return cell!
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueOneConnect" {
            let index = sender as! Int
            if let vc = segue.destination as? oneConnectViewController {
                vc.id = self.connects![index].id
                vc.connect = self.connects![index]
                if self.connects!.count == 1 {
                    vc.noBackButton = true
                    
                }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.performSegue(withIdentifier: "segueOneConnect", sender: indexPath.row)
        
    }
 

}

extension Date {
    func getElapsedInterval() -> String {
        
        var calendar = Calendar.current
        calendar.locale = Locale(identifier: Bundle.main.preferredLocalizations[0])
        // IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN
        // WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE
        // (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME
        // IS GOING TO APPEAR IN SPANISH
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.maximumUnitCount = 1
        formatter.calendar = calendar
        
        var dateString: String?
        
        let interval = calendar.dateComponents([.year, .month, .weekOfYear, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            formatter.allowedUnits = [.year] //2 years
        } else if let month = interval.month, month > 0 {
            formatter.allowedUnits = [.month] //1 month
        } else if let week = interval.weekOfYear, week > 0 {
            formatter.allowedUnits = [.weekOfMonth] //3 weeks
        } else if let day = interval.day, day > 0 {
            formatter.allowedUnits = [.day] // 6 days
        } else if let hour = interval.hour, hour > 0 {
            formatter.allowedUnits = [.hour] // 6 hour
        } else if let minute = interval.minute, minute > 0 {
            formatter.allowedUnits = [.minute] // 6 days
        } else if let second = interval.second, second > 0 {
            formatter.allowedUnits = [.second] // 6 days
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: Bundle.main.preferredLocalizations[0]) //--> IF THE USER HAVE THE PHONE IN SPANISH BUT YOUR APP ONLY SUPPORTS I.E. ENGLISH AND GERMAN WE SHOULD CHANGE THE LOCALE OF THE FORMATTER TO THE PREFERRED ONE (IS THE LOCALE THAT THE USER IS SEEING THE APP), IF NOT, THIS ELAPSED TIME IS GOING TO APPEAR IN SPANISH
            dateFormatter.dateStyle = .medium
            dateFormatter.doesRelativeDateFormatting = true
            
            dateString = dateFormatter.string(from: self) // IS GOING TO SHOW 'TODAY'
        }
        
        if dateString == nil {
            dateString = formatter.string(from: self, to: Date())! + NSLocalizedString(" ago", comment: "ago")
        }
        
        return dateString!
    }
    
}

