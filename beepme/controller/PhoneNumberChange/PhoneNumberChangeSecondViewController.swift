//
//  PhoneNumberChangeSecondViewController.swift
//  beepme
//
//  Created by Раиль Абдуллин on 04.09.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import UIKit

class PhoneNumberChangeSecondViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    var newPhone: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveButtonTouchUpInside(_ sender: UIButton) {
        sender.isEnabled = false
        
        let userAPIService = APIAuthorizationService()

        let code = textField.text
        
        if code != nil {
            userAPIService.phoneUpdateFinish (newPhone: newPhone!, code: code!) { (result) -> () in
                print("result", result)
                sender.isEnabled = true

                if result == "ok" {
                    DispatchQueue.main.async {
                        self.presentingViewController?.dismiss(animated: true, completion: nil)
                        self.dismiss(animated: true, completion: nil)
                        //self.navigationController?.popToRootViewController(animated: true)
                        

                    }
                    
                }
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
