//
//  PhoneChangeFirstViewController.swift
//  beepme
//
//  Created by Раиль Абдуллин on 04.09.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import UIKit

class PhoneChangeFirstViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendCodeButtonTouchUpInside(_ sender: UIButton) {
        sender.isEnabled = false

        let userAPIService = APIAuthorizationService()
        let newPhone = textField.text
        
        if newPhone != nil {
            userAPIService.phoneUpdateSendCode (newPhone: newPhone!) { (result) -> () in
                print("result", result)
                sender.isEnabled = true

                if result == "ok" {
                    self.performSegue(withIdentifier: "segueSecond", sender: newPhone)
                } else if result == "exist" {
                    let alert = UIAlertController(title: "Используется", message: "Указанный номер телефона используется другим пользователем", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хорошо, попробую другой", style: .default, handler: nil ))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
        
        
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSecond" {
            let newPhone = sender as! String
            if let vc = segue.destination as? PhoneNumberChangeSecondViewController {
                vc.newPhone = newPhone
                
            }
        }
    }
    

}
