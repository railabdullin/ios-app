//
//  ViewController.swift
//  beepme
//
//  Created by Macbook on 19/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import UserNotifications

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIActionSheetDelegate {
    
    
    @IBOutlet weak var carNumberTF: UITextField!
    @IBOutlet weak var regionTF: UITextField!
    
    @IBOutlet weak var zonePicker: UIPickerView!
    
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var carNumberView: UIView!
    
    
    var newMessegesCount = 0
    var hasActiveDialog = false
    var firstNumberChange = true
    var firstStart = true
    var lastConnectID: Int?
    
    var pushTimer = Timer()
    var autoCheckTimer = Timer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        carNumberTF.setNoBorder()
        regionTF.setNoBorder()
        carNumberView.stylyzateCarNumberView()
        
        carNumberTF.delegate = self
        
        zonePicker.dataSource = self
        zonePicker.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        //уведомления
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {
            (granted, error) in
            if granted {
                print("yes")
            } else {
                print("No")
            }
        }
        
        // #1.1 - Create "the notification's category value--its type."
        let debitOverdraftNotifCategory = UNNotificationCategory(identifier: "debitOverdraftNotification", actions: [], intentIdentifiers: [], options: [])
        // #1.2 - Register the notification type.
        UNUserNotificationCenter.current().setNotificationCategories([debitOverdraftNotifCategory])
        
        carNumberTF.addTarget(self, action: #selector(numberChange), for: .editingChanged)
        carNumberTF.addTarget(self, action: #selector(numberEndEditing), for: .editingDidEnd)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let userAPIService = APIAuthorizationService()
        
        userAPIService.get_user_data(){ [self] (result) -> () in
            if result?.active_connect_id != nil {
                self.performSegue(withIdentifier: "segueOneConnect", sender: result!.active_connect_id!)
            }
            
            // Если у пльзователя нет автомобиля? то открываем страницу добавления автомобиля
            if result?.vehicles?.count == 0 && self.firstStart {
                firstStart = false
                self.performSegue(withIdentifier: "segueAccount", sender: nil)
            }
        }
        
        autoCheckTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(checkNewMessages), userInfo: nil, repeats: true)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    @objc func checkNewMessages() {
        /*
        var responseString = "null"
        
        let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path)
        
        let needEthernet = true
        let url = URL(string: dict!.object(forKey: "url") as! String )!
        var request2 = URLRequest(url: url)
        request2.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request2.httpMethod = "POST"
        var postString2 = "k=\(dict!.object(forKey: "k")!)&command=get_start_info&phone=\(userData.phone!)&sessionKey=\(userData.sessionKey!)"
        
        print("postString=\(postString2)")
        
        request2.httpBody = postString2.data(using: .utf8)
        
        let task2 = URLSession.shared.dataTask(with: request2) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error ?? "null" as! Error)")
                if needEthernet==true {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Internet connection required", message: "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Oh, something went wrong", message: "We received error information and will fix it soon.", preferredStyle: UIAlertController.Style.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        (UIAlertAction) -> Void in
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            responseString = String(data: data, encoding: .utf8) ?? "null"
            print(responseString)
            
            if  responseString != "" {
                DispatchQueue.main.async {
                    do {
                        let decoder = JSONDecoder()
                        let decodedJSON = try? decoder.decode(jsonKeys.authorization.self, from: data)
                        dump(decodedJSON)
                        
                        if decodedJSON?.success == true {
                            print("1111")
                            let defaults = UserDefaults.standard
                            defaults.set(decodedJSON?.sck, forKey: "sessionKey")
                            
                            
                            let newMessegesCount = (decodedJSON?.new_messeges_count)!
                            self.hasActiveDialog = (decodedJSON?.has_last_connect)!
                            self.lastConnectID = decodedJSON?.last_connect_id
                            
                            
                            if newMessegesCount != 0 {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = "\(newMessegesCount)"
                                }
                                
                                //push уведомления
                                var location: CLLocation?
                                if decodedJSON?.new_messege_location_longitude != nil && decodedJSON?.new_messege_location_lattitude != nil {
                                    location = CLLocation(latitude: Double( (decodedJSON?.new_messege_location_lattitude!)!) as! CLLocationDegrees, longitude: Double( (decodedJSON?.new_messege_location_longitude!)!) as! CLLocationDegrees)
                                }
                                
                                
                                //self.pushNotification(location: location, stickerID: (decodedJSON?.new_messege_sticker_id!)!, carNumber: (decodedJSON?.new_messege_car_number!)!)
                                //self.pushTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: Selector(("pushNotification")), userInfo: nil, repeats: false)
                                
                            } else {
                                
                            }
                            
                            //если нет машин то показать уведомление об этом с призывов добавить транспорт
                            if decodedJSON?.car_numbers != nil {
                                accountViewController.carNumbers = (decodedJSON?.car_numbers)!
                            }
                            
                            if decodedJSON?.blacklist != nil {
                                blacklistViewController.blacklist = (decodedJSON?.blacklist)!
                            }
                            
                        } else {
                            
                            if decodedJSON?.code == 530 {
                                userData.phone = ""
                                userData.sessionKey = ""
                                let action = authorizationAllViewController()
                                action.saveToFile()
                            } else {
                                let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: decodedJSON?.description , preferredStyle: UIAlertController.Style.alert)
                                let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                                    (UIAlertAction) -> Void in
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                            errorLoging.serverLog(errorText: responseString, command: "get_start_info")
                        }
                    }
                }
            }
        }
        task2.resume()
 */
    }
    
    func getDataFromServer(_ completion: @escaping () -> Void){
        /*
        var responseString = "null"
        
        let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path)
        
        let needEthernet = true
        let url = URL(string: dict!.object(forKey: "url") as! String )!
        var request2 = URLRequest(url: url)
        request2.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request2.httpMethod = "POST"
        var postString2 = "k=\(dict!.object(forKey: "k")!)&command=get_start_info&phone=\(userData.phone!)&sessionKey=\(userData.sessionKey!)"
        
        //если есть notification device token то отправляем его на сервер
        if userData.notificationDeviceToken != nil && userData.notificationDeviceToken != ""{
            postString2 += "&ntftoken=" + userData.notificationDeviceToken!
        }
        
        print("postString=\(postString2)")
        
        request2.httpBody = postString2.data(using: .utf8)
        
        let task2 = URLSession.shared.dataTask(with: request2) { data, response, error in
            guard let data = data, error == nil else { // check for fundamental networking error
                print("error=\(error ?? "null" as! Error)")
                if needEthernet==true {
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Internet connection required", message: "", preferredStyle: UIAlertController.Style.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Oh, something went wrong", message: "We received error information and will fix it soon.", preferredStyle: UIAlertController.Style.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                        (UIAlertAction) -> Void in
                    }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            responseString = String(data: data, encoding: .utf8) ?? "null"
            print(responseString)
            
            if  responseString != "" {
                DispatchQueue.main.async {
                    do {
                        let decoder = JSONDecoder()
                        let decodedJSON = try? decoder.decode(jsonKeys.authorization.self, from: data)
                        dump(decodedJSON)
                        
                        if decodedJSON?.success == true {
                            print("1111")
                            let defaults = UserDefaults.standard
                            defaults.set(decodedJSON?.sck, forKey: "sessionKey")
                            
                            
                            let newMessegesCount = (decodedJSON?.new_messeges_count)!
                            self.hasActiveDialog = (decodedJSON?.has_last_connect)!
                            self.lastConnectID = decodedJSON?.last_connect_id
                            
                            if self.hasActiveDialog {
                                self.performSegue(withIdentifier: "segueOneConnect", sender: nil)
                            }
                            
                            if newMessegesCount != 0 {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = "\(newMessegesCount)"
                                   }
                                
                                //push уведомления
                                var location: CLLocation?
                                if decodedJSON?.new_messege_location_longitude != nil && decodedJSON?.new_messege_location_lattitude != nil {
                                    location = CLLocation(latitude: Double( (decodedJSON?.new_messege_location_lattitude!)!) as! CLLocationDegrees, longitude: Double( (decodedJSON?.new_messege_location_longitude!)!) as! CLLocationDegrees)
                                }
                                
                                
                                //self.pushNotification(location: location, stickerID: (decodedJSON?.new_messege_sticker_id!)!, carNumber: (decodedJSON?.new_messege_car_number!)!)
                                //self.pushTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: Selector(("pushNotification")), userInfo: nil, repeats: false)
                                
                            } else {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = nil
                                }
                            }
                            
                            if decodedJSON?.car_numbers != nil {
                                accountViewController.carNumbers = (decodedJSON?.car_numbers)!
                            } else {
                                self.showActionSheet()
                            }
                            
                            if decodedJSON?.blacklist != nil {
                                blacklistViewController.blacklist = (decodedJSON?.blacklist)!
                            }
                            
                        } else {
                            
                            if decodedJSON?.code == 530 {
                                userData.phone = ""
                                userData.sessionKey = ""
                                let action = authorizationAllViewController()
                                action.saveToFile()
                            } else {
                                let alert = UIAlertController(title: NSLocalizedString("Error", comment: ""), message: decodedJSON?.description , preferredStyle: UIAlertController.Style.alert)
                                let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                                    (UIAlertAction) -> Void in
                                }
                                alert.addAction(alertAction)
                                self.present(alert, animated: true, completion: nil)
                                
                                errorLoging.serverLog(errorText: responseString, command: "get_start_info")
                            }
                            
                            
                            
                        }
                    }
                }
            }
            completion()
        }
        task2.resume()
 */
    }
    
    @objc func pushNotification(location: CLLocation?, stickerID : Int, carNumber: String) {
        print("push")
        
        // 1
        let content = UNMutableNotificationContent()
        content.title = "Новое сообщение"
        content.subtitle = "Номер авто: \(carNumber)"
        content.body = stickers.names[stickerID]
        content.sound = .default
        // 2
        
        //делаем скиншот карты
        let mapSnapshotOptions = MKMapSnapshotter.Options()
        
        // Set the region of the map that is rendered.
        if location != nil {
            let region = MKCoordinateRegion(center: location!.coordinate, latitudinalMeters: 50, longitudinalMeters: 50)
            mapSnapshotOptions.region = region
            
        }
        
        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
        mapSnapshotOptions.scale = UIScreen.main.scale
        
        // Set the size of the image output.
        mapSnapshotOptions.size = CGSize(width: 300, height: 300)
        
        // Show buildings and Points of Interest on the snapshot
        mapSnapshotOptions.showsBuildings = true
        mapSnapshotOptions.showsPointsOfInterest = true
    
        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
        
        snapShotter.start { (snapshot:MKMapSnapshotter.Snapshot?, error:Error?) in
            
            //рисуем точку
            UIGraphicsBeginImageContextWithOptions(mapSnapshotOptions.size, true, 0)
            snapshot!.image.draw(at: .zero)
            
            let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
            let pinImage = pinView.image
            
            var point = snapshot!.point(for: location!.coordinate)
            
            
            let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: mapSnapshotOptions.size)
            if rect.contains(point) {
                let pinCenterOffset = pinView.centerOffset
                point.x -= pinView.bounds.size.width / 2
                point.y -= pinView.bounds.size.height / 2
                point.x += pinCenterOffset.x
                point.y += pinCenterOffset.y
                pinImage?.draw(at: point)
            }
            
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            
            //получаем картинку
            //let image = snapshot?.image
            
            
            //записываем картинку в кэш
            let photoName = "fileName.png"
            let data = image!.pngData()
            let directory = try? FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL
            
            do {
                try data!.write(to: directory!.appendingPathComponent(photoName)!)
            } catch {
                print(error.localizedDescription)
            }
            
            
            //получаем ссылку на картинку
            let imageURL: URL?
            if let dir = try? FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false) {
                imageURL = URL(fileURLWithPath: dir.absoluteString).appendingPathComponent(photoName)
                
                
                let attachment = try! UNNotificationAttachment(identifier: photoName, url: imageURL!, options: .none)
                print(attachment)
                
                content.attachments = [attachment]
                content.categoryIdentifier = "debitOverdraftNotification"

                
                // 3
                let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
                let request = UNNotificationRequest(identifier: "notification.id.01", content: content, trigger: trigger)
                
                // 4
                UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
            }
            
        }
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return carNumberZones.zones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return carNumberZones.zones[row]["displayName"]
    }

    @IBAction func search(_ sender: Any) {
        let _carNumber = "\(carNumberTF.text!) \(regionTF.text!)"
        if carNumberTF.text != nil && carNumberTF.text != "" {
            let _carNumber = carNumberTF.text! + " " + regionTF.text!
            let _zone = carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["systemName"]
            
            let vehicleAPIServices = VehicleApiService()
            
            vehicleAPIServices.search(locale_code: _zone, carnumber: _carNumber, callback: {(result) -> () in
                print("result ", result)
                if result?.count != 0 {
                    print("show")
                    self.performSegue(withIdentifier: "segueSearchResult", sender: [result![0].number , result![0].id!, result![0].hex_color!])
                } else {
                    let alert = UIAlertController(title: "Не найдено", message: "Не удалось найти транспортное средство", preferredStyle: UIAlertController.Style.alert)
                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                            (UIAlertAction) -> Void in
                        }
                    alert.addAction(alertAction)
                    self.present(alert, animated: true, completion: nil)
                        
                    
                }
            })
            
            /*
            
            var responseString = "null"
            
            let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
            let dict = NSDictionary(contentsOfFile: path)
            
            let needEthernet = true
            let url = URL(string: dict!.object(forKey: "url") as! String )!
            var request2 = URLRequest(url: url)
            request2.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            request2.httpMethod = "POST"
            let postString2 = "k=\(dict!.object(forKey: "k")!)&command=search&phone=\(userData.phone!)&sessionKey=\(userData.sessionKey!)&zone=\(carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)] )&car_number=\(_carNumber)"
            
            print("postString=\(postString2)")
            
            request2.httpBody = postString2.data(using: .utf8)
            
            let task2 = URLSession.shared.dataTask(with: request2) { data, response, error in
                guard let data = data, error == nil else { // check for fundamental networking error
                    print("error=\(error ?? "null" as! Error)")
                    if needEthernet==true {
                        DispatchQueue.main.async {
                            self.activityIndicator.stopAnimating()
                            let alert = UIAlertController(title: NSLocalizedString("Internet connection required", comment: "" ) , message: "", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                    return
                }
                
                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 { // check for http errors
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        let alert = UIAlertController(title: "Oh, something went wrong", message: "We received error information and will fix it soon.", preferredStyle: UIAlertController.Style.alert)
                        let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                            (UIAlertAction) -> Void in
                        }
                        alert.addAction(alertAction)
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                }
                responseString = String(data: data, encoding: .utf8) ?? "null"
                print(responseString)
                
                if  responseString != "" {
                    DispatchQueue.main.async {
                        self.activityIndicator.stopAnimating()
                        do {
                            let decoder = JSONDecoder()
                            let decodedJSON = try? decoder.decode(jsonKeys.searchResult.self, from: data)
                            dump(decodedJSON)
                            
                            print(decodedJSON?.success)
                            if decodedJSON?.success == true {
                                self.performSegue(withIdentifier: "segueSearchResult", sender: [decodedJSON?.car_number!, decodedJSON?.author_id!, decodedJSON?.hex_color!])
                            } else {
                                if decodedJSON?.code == 530 {
                                    
                                    userData.phone = ""
                                    userData.sessionKey = ""
                                    
                                    let action = authorizationAllViewController()
                                    action.saveToFile()
                                    
                                } else {
                                    let alert = UIAlertController(title: NSLocalizedString("Error", comment: "" ), message: decodedJSON?.description , preferredStyle: UIAlertController.Style.alert)
                                    let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                                        (UIAlertAction) -> Void in
                                    }
                                    alert.addAction(alertAction)
                                    self.present(alert, animated: true, completion: nil)
                                    
                                    errorLoging.serverLog(errorText: responseString, command: "search")
                                }
                                
                                
                            }
                        }
                    }
                }
            }
            task2.resume()
 */
            
            
        } else {
            let alert = UIAlertController(title: NSLocalizedString("Пожалуйста, введите гос. номер", comment: ""), message: "" , preferredStyle: UIAlertController.Style.alert)
            let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){
                (UIAlertAction) -> Void in
            }
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueSearchResult" {
            let array = sender as! [Any]
            let carNumber = array[0]
            let vehicleID = array[1]
            let hexColor = array[2]
            
            if let vc = segue.destination as? searchResultViewController {
                vc.vehicleID = vehicleID as! Int
                vc.carNumber = carNumber as! String
                vc.hexColor = hexColor as! String
            }
        } else if segue.identifier == "segueOneConnect" {
            if let vc = segue.destination as? oneConnectViewController {
                vc.id = sender as! Int?
            }
        }
    }
    
    //закрыть клавиатуру если нажать return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        self.search("")
        
        return false
    }
    
    @objc func numberChange(){
        var _carNumber = carNumberTF.text
        
        if carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["systemName"] == "ru" {
            
            if _carNumber!.count == 6 && firstNumberChange {
                if !(_carNumber?.contains(" "))! {
                    //carNumberTF.resignFirstResponder()
                    regionTF.becomeFirstResponder()
                    firstNumberChange = false
                }
                
            }
            
            /*else if _carNumber!.count == 10 && firstNumberChange {
                if !(_carNumber?.contains(" "))! {
                    carNumberTF.resignFirstResponder()
                    firstNumberChange = false
                }
            }*/

        }
    }
    
    @objc func numberEndEditing(){
        print("numberEndEditing")
        var _carNumber = carNumberTF.text
        
        if carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["systemName"] == "ru" && _carNumber!.count == 9 {
            
            //если пользователь уже не поставил пробел
            if !(_carNumber?.contains(" "))! {
                
                var index = _carNumber!.index(_carNumber!.startIndex, offsetBy: 6)
                let substringFirst = String(_carNumber![..<index])
                print(substringFirst)
                
                index = _carNumber!.index(_carNumber!.endIndex, offsetBy: -3)
                let substringSecond = _carNumber![index...] // playground
                
                print(substringSecond)
                
                _carNumber = substringFirst + " " + substringSecond
                carNumberTF.text = _carNumber!
            }
        }
    }
    
    
    //только маленькие буквы и цифры в TF
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("маленькие")
        if string.isEmpty {
            return true
        }
        let regex = "[а-я, a-z, 0-9]{1,}"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: string)
    }
    
    func showActionSheet()
    {
        
        let actionSheetController = UIAlertController(title: "Для поиска зарегистрируйте транспортное средство", message: nil, preferredStyle: .actionSheet)
        
        // Create and add the Cancel action
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { action -> Void in
            // Just dismiss the action sheet
        }
        actionSheetController.addAction(cancelAction)
        
        // Create and add first option action
        let takePictureAction = UIAlertAction(title: "Добавить", style: .default) { action -> Void in
            self.performSegue(withIdentifier: "segueAccount", sender: nil)
        }
        actionSheetController.addAction(takePictureAction)
       
        // We need to provide a popover sourceView when using it on iPad
        //actionSheetController.popoverPresentationController?.sourceView = sender as UIView
        
        // Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
   
}

