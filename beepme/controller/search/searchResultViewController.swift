//
//  searchResultViewController.swift
//  beepme
//
//  Created by Macbook on 29/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class searchResultViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate {
    
    @IBOutlet weak var carImage: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var numberOfItemsPerRow = 3
    
    var vehicleID: Int?
    var carNumber: String?
    var hexColor: String?
    var firstSend = true
    
    let stickersForSending = [1]
    let stikersNames = ["Звонок"]
    
    var locationManager: CLLocationManager!
    
    var tappedStickerID: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        carImage.backgroundColor = hexStringToUIColor(hex: self.hexColor!)
        collectionView.reloadData()
        
        firstSend = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if locationManager != nil {
            locationManager.stopUpdatingLocation()
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        //если нашел местоположение то отключаем отслеживание и отправляем стикер
        locationManager.stopUpdatingLocation()
        if firstSend {
            sendSticker(location: location)
            firstSend = false
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if stickersForSending != nil {
            return (stickersForSending.count)
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as? stickersCollectionViewCell
        
        if stickersForSending[indexPath.row] != 0 {
            let stickerName = "sticker\(stickersForSending[indexPath.row])"
            print(stickerName)
            itemCell!.imageView.image = UIImage(named: stickerName)
            itemCell?.title.text = stikersNames[indexPath.row]
            
            return itemCell!
        } else {
            return UICollectionViewCell()
        }
        
    }
    
    //задаем ширину ячеек в collectionView
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: size + 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let stickerID = stickersForSending[indexPath.row]
        tappedStickerID = stickerID
        //запрашиваем местоположение
        let alert = UIAlertController(title: NSLocalizedString("Location", comment: "Location") , message:  NSLocalizedString("Сейчас мы будем использовать ваше местоположение, чтобы отобразить его владельцу автомобиля. Отправка стикера без вашего местоположения невозможна.", comment: "Сейчас мы будем использовать ваше местоположение, чтобы отобразить его владельцу автомобиля. Отправка стикера без вашего местоположения невозможна."), preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("Хорошо, я понял.", comment: "Хорошо, я понял"), style: UIAlertAction.Style.default){
            (UIAlertAction) -> Void in
            if (CLLocationManager.locationServicesEnabled())
            {
                self.locationManager = CLLocationManager()
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
                self.locationManager.requestAlwaysAuthorization()
                self.locationManager.startUpdatingLocation()
            }
        }
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func sendSticker(location : CLLocation) {
        let connectAPIService = ConnectsApiService()
        
        LoadingIndicator.shared.startAnimation()
        
        connectAPIService.createConnect(vehicleID: vehicleID!, stickerID: tappedStickerID!, location: location, callback: { (result) in
            DispatchQueue.main.async {
                LoadingIndicator.shared.stopAnimation()
                print("res ", result)
                if result! == "ok" {
                    
                    self.dismissVC()

                    
                } else if result! == "limit" {
                    
                    let alert = UIAlertController(title: "Лимит", message: "Вы связывались с данным пользователем за последние 2 часа. Должно пройти более 2 часов", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Хорошо, я понял", style: .default, handler: {
                        _ in
                        self.dismissVC()
                    } ))
                    self.present(alert, animated: true, completion: nil)
                    
                } else if result! == "havenotcar" {
                    
                    let alert = UIAlertController(title: "Нет автомобиля", message: "Добавьте автомобиль", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Понял", style: .default, handler: {
                        _ in
                        self.dismissVC()
                    } ))
                    self.present(alert, animated: true, completion: nil)
                    
                } else if result! == "blocked" {
                    
                    let alert = UIAlertController(title: "Заблокировано", message: "Пользователь запретил Вам связываться с ним", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Хорошо, я понял", style: .default, handler: {
                        _ in
                        self.dismissVC()
                    } ))
                    self.present(alert, animated: true)
                                    }
                
            }
        })
    }
    
    func dismissVC()   {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}

