//
//  blacklist.swift
//  beepme
//
//  Created by Macbook on 20/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit

class blacklistViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var blacklist : [BlacklistItem]?
    let defaults = UserDefaults.standard
    
    var page = 1
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = 50
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getBlacklist()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(blacklist)
        if blacklist != nil {
            
            print("blacklist!.count \(blacklist!.count)")
            return blacklist!.count
        } else {
            print("blacklist!.count = 0")
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell")
        
        if blacklist![indexPath.row].blocked_vehicle != nil {
            cell?.textLabel!.text = "Гос. номер:  \(blacklist![indexPath.row].blocked_vehicle!.number!)"
        } else {
            cell?.textLabel!.text = "Гос. номер неизвестен"
        }
            
        
        
        //cell?.imageView?.image = UIImage(named: "rect")
        if blacklist![indexPath.row].blocked_vehicle?.hex_color != nil {
            
            print("color")
            let colorConvertor = hexStringtoUIColor()
            cell?.imageView?.backgroundColor = colorConvertor.hexStringToUIColor(hex: blacklist![indexPath.row].blocked_vehicle!.hex_color!)
            
        }
        
        if blacklist![indexPath.row].time != nil {
            cell?.detailTextLabel!.text = DateTimeFormatter.getDateTime(stringDate:  blacklist![indexPath.row].time!  )
        } else {
            cell?.detailTextLabel!.text = ""
        }
        
        return cell!
    }
    
    //удаляем из блокированных
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            let id =  blacklist![indexPath.row].id
            
            let blackAPIService = BlacklistApiService()
            blackAPIService.delete(id: id!) { (result) -> () in
                if result == "ok" {
                    self.blacklist?.remove(at: indexPath.row)
                    self.tableView.deleteRows(at: [indexPath], with: .left)
                }
            }
            
            
            
            
            
            
        }
    }
    
    @IBAction func refresh(_ sender: UIBarButtonItem) {
        self.getBlacklist()
    }
    
    func getBlacklist() {
        let blackAPIService = BlacklistApiService()
        blackAPIService.get_list(page: page) { (result) -> () in
            self.blacklist = result
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
