//
//  accountViewController.swift
//  beepme
//
//  Created by Macbook on 20/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import SemiModalViewController

class accountViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var account: user?

    var vehicles : [Vehicle]?
    var firstLaunch = true
    var creditCardMusk: String?
    let userAPIService = APIAuthorizationService()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        //self.tableView.rowHeight = 102
        
        navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "#165d73")
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {        
        userAPIService.get_user_data() { (result) -> () in
            print("result", result)
            self.creditCardMusk = result?.credit_card_mask
            self.vehicles = result?.vehicles
            self.tableView.reloadData()
        }
        
        /*
         
         accountViewController.carNumbers = decodedJSON?.car_numbers
         self.tableView.reloadData()
         
         //если машин нет и первый раз открыли экран, то открываем экран добавление номера
         print(accountViewController.carNumbers)
         if accountViewController.carNumbers != nil {
             print("1 \(accountViewController.carNumbers!.count)")
             if accountViewController.carNumbers!.count == 0 {
                 if self.firstLaunch {
                     self.firstLaunch = false
                     self.performSegue(withIdentifier: "segueAddCar", sender: nil)
                 }
             }
         } else {
             print("2")
             if self.firstLaunch {
                 self.firstLaunch = false
                 self.performSegue(withIdentifier: "segueAddCar", sender: nil)
             }
         }
         
         */
    }
    
    @IBAction func logout(_ sender: UIBarButtonItem) {
    }
    
    //MARK: tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Мои ТС"
        case 1:
            return "Автопополнение"
        case 2:
            return "Способы оплаты"
        case 3:
            return "Номер телефона"
        default:
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            // ячейка для авто
            if vehicles != nil && vehicles!.count != 0 {
                
                if vehicles!.count < 3 {
                    return vehicles!.count + 1
                } else {
                    return 3
                }
            } else {
                return 1
            }
        case 1:
            return 1
        case 2:
            return 1
        case 3:
            return 1
        case 4:
            // кнопка поддержки и о приложении
            return 2

        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! carNumbersTableViewCell
            
            if vehicles == nil || (vehicles!.count < 3 && indexPath.row == vehicles!.count) {
                cell.titleLabel.text = "Добавить ТС"
                let label = UILabel(frame: cell.smallView.frame)
                label.text = "+"
                label.center =  cell.smallView.center
                label.textAlignment = NSTextAlignment.center
                
                cell.smallView.addSubview(label)
                
                cell.tag = 1
            } else {
                cell.titleLabel.text = self.vehicles![indexPath.row].number!
                cell.smallView.backgroundColor = self.hexStringToUIColor(hex: vehicles![indexPath.row].hex_color!)
                cell.tag = 0
            }
            cell.backgroundColor = hexStringToUIColor(hex: "#f7f7f7")
            cell.layer.cornerRadius = 5
            
            return cell
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell")
            cell?.textLabel?.text = "0,00 руб"
            cell?.textLabel?.textColor = .gray
            cell!.backgroundColor = hexStringToUIColor(hex: "#f7f7f7")
            cell?.layer.cornerRadius = 5

            return cell!
            
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell")
            if self.creditCardMusk != nil {
                cell?.textLabel?.text = self.creditCardMusk!
                cell?.imageView?.image = UIImage(named: "mastercard")
            } else {
                cell?.textLabel?.text = "Добавить карту"
                if #available(iOS 13.0, *) {
                    cell?.imageView?.image = UIImage(systemName: "plus")
                } else {
                    cell?.imageView?.image = UIImage(named: "plus")
            }
            
            
            cell?.textLabel?.textColor = .gray
                       }
            // UIImage(named: "mastercard")
            cell!.backgroundColor = hexStringToUIColor(hex: "#f7f7f7")
            cell?.layer.cornerRadius = 5

           
            return cell!
            
        case 3:
            let defaults = UserDefaults.standard
            var phone = defaults.string(forKey: "phone")
            
            let phoneMaskHelper = PhoneMaskHelper()
            phone = phoneMaskHelper.mask(phone: phone!)
            
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell")
            cell?.textLabel?.text = phone
            cell?.textLabel?.textColor = .gray
            cell!.backgroundColor = hexStringToUIColor(hex: "#f7f7f7")
            cell?.layer.cornerRadius = 5

            return cell!
            
        case 4:
            switch indexPath.row {
            case 0:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ButtonTableViewCell
             
                cell.button.setTitle("Поддержка", for: .normal)
                cell.button.setTitleColor(.white, for: .normal)
                cell.button.addTarget(self , action: #selector(supportCLick), for: .touchUpInside)
                cell.button.layer.cornerRadius = 10
                cell.button.layer.borderWidth = 1
                cell.button.layer.borderColor = UIColor.black.cgColor
                return cell
                
            case 1:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "linkCell", for: indexPath) as! LinkTableViewCell

                cell.button.addTarget(self , action: #selector(aboutClick), for: .touchUpInside)
                cell.button.setTitle("О приложении", for: .normal)
                cell.button.layer.cornerRadius = 10
                cell.button.layer.borderWidth = 0
                cell.button.layer.borderColor = .none
                cell.button.backgroundColor = .none
                cell.button.setTitleColor(hexStringToUIColor(hex: "#9A9A9A"), for: .normal)
                return cell
                
                
            default:
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "buttonCell", for: indexPath) as! ButtonTableViewCell
                return cell

            }
            
        
            
        default:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "basicCell")
            cell!.backgroundColor = hexStringToUIColor(hex: "#f7f7f7")

            return cell!
        }
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.section {
        case 0:
            return 102
        case 4:
            return 80
        default:
            return 56
        }
    }
    
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 0:
            if tableView.cellForRow(at: indexPath)?.tag == 1 {
                self.performSegue(withIdentifier: "segueAddCar", sender: nil)
            } else {
                self.performSegue(withIdentifier: "segueCarNumber", sender: vehicles![indexPath.row].id!)
            }
        case 1:
            // Баланс
            /*
            let options = [
                SemiModalOption.pushParentBack: false
            ]

            //let controller = BalanceModalViewController()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "balanaceModalView")
            
            controller.view.bounds.size.height = 350
            //controller.view.backgroundColor = .red

            presentSemiViewController(controller, options: options, completion: {
                print("Completed!")
            }, dismissBlock: {
                print("Dismissed!")
            })
            */
            //self.performSegue(withIdentifier: "segueAutobalanceModal", sender: nil)
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "balanaceModalView")
            
            controller.view.bounds.size.height = 350
            controller.modalPresentationStyle = .pageSheet
            present(controller, animated: true, completion: nil)
            

            break
            
        case 2:
            // банковская карта
            
            
                if creditCardMusk == nil {
                    self.performSegue(withIdentifier: "seguaCardRegister", sender: nil)
                } else {

                    let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)

                    alert.addAction(UIAlertAction(title: "Изменить карту", style: .default , handler:{ (UIAlertAction)in
                        self.performSegue(withIdentifier: "seguaCardRegister", sender: nil)
                    }))

                    alert.addAction(UIAlertAction(title: "Удалить карту", style: .destructive , handler:{ (UIAlertAction)in
                        self.userAPIService.BankCardDelete() { (result) -> () in
                            print("result", result)
                            if result == "ok"{
                                self.creditCardMusk = nil
                                self.tableView.reloadData()
                            }
                        }
                    }))

                    alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
                        print("User click Dismiss button")
                    }))

                    self.present(alert, animated: true, completion: {
                        print("completion block")
                    })
                }
            
            break
            
        case 3:
            // телефон
            let alert = UIAlertController(title: "Выберите действие", message: nil, preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: "Изменить телефон", style: .default , handler:{ (UIAlertAction)in
                self.performSegue(withIdentifier: "seguePhoneChange", sender: nil)
            }))


            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
                print("User click Dismiss button")
            }))

            self.present(alert, animated: true, completion: {
                print("completion block")
            })
            break
            
        default:
            break
        }
        
        
        

    }
    
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueCarNumber" {
            let id = sender as? Int
            print("id=\(id)")
           
            if let vc = segue.destination as? editCarNumberViewController {
                vc.id = id
            }
        }
    }
    
    @IBAction func logoutButton(_ sender: Any) {
        
        let alert = UIAlertController(title: "Вы действительно хотите выйти из аккаунта?", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Выйти", style: .destructive , handler:{ (UIAlertAction) in
            let defaults = UserDefaults.standard
            defaults.set(nil, forKey: "token")
            defaults.set(nil, forKey: "phone")
            defaults.set(nil, forKey: "blocked")
            defaults.set(nil, forKey: "my_id")
            defaults.set(nil, forKey: "newMessagesCount")

            let vc = UIStoryboard(name: "Main", bundle: Bundle.main)
            let mainController = vc.instantiateViewController(withIdentifier: "authControler")
            self.navigationController?.navigationBar.isHidden = true
            self.tabBarController?.setViewControllers([], animated: false)
            self.navigationController?.setViewControllers([mainController], animated: false)
        }))

        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })
        
        
    }
    
    @objc func supportCLick(){
        let subject = "Обращение в поддержку"
        let systemVersion = UIDevice.current.systemVersion
        let body = """

Информация для отладки:
Устройство \(UIDevice().type)
Версия iOS \(systemVersion)
"""
        let coded = "mailto:numpass@yandex.ru?subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        if let emailURL: NSURL = NSURL(string: coded!) {
            if UIApplication.shared.canOpenURL(emailURL as URL) {
                UIApplication.shared.openURL(emailURL as URL)
            }
        }
    }
    
    @objc func aboutClick(){
        self.performSegue(withIdentifier: "segueAbout", sender: nil)
    }
}
