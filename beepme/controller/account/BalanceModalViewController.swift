//
//  BalanceModalViewController.swift
//  beepme
//
//  Created by Раиль Абдуллин on 12.08.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import UIKit

class BalanceModalViewController: UIViewController {
    
    @IBOutlet var myview: UIView!
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var cashLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.preferredContentSize = CGSize(width: 100, height: 100)
        
        self.hideKeyboardWhenTappedAround()

        //чтобы двигать view вместе с клавиатурой
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        let autobalance = defaults.integer(forKey: "autobalance")
        let cash = defaults.integer(forKey: "cash")
        
        if autobalance != nil {
            textField.text = String(autobalance)
        }
        
        if cash != nil {
            cashLabel.text = String(cash)
        }
    }
    
    //чтобы двигать view вместе с клавиатурой
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func closeTouchUp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButtonTouchUpInside(_ sender: Any) {
        let text = textField.text
        
        if text != nil {
            let value = Int(text!)!
            if value >= 300 {
                let userAPIService = APIAuthorizationService()
                userAPIService.update_autobalance(value: value) { (result) -> () in
                    print("result", result)
                    let defaults = UserDefaults.standard
                    defaults.set(value, forKey: "autobalance")
                    defaults.synchronize ()
                    
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Минимальная сумма", message: "Сумма автобаланса должна быть больше 300 руб.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Сумма автобаланса должна быть указана", message: "", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    


}
