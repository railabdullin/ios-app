//
//  editCarNumberViewController.swift
//  beepme
//
//  Created by Macbook on 20/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit

class editCarNumberViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var carNumberTF: UITextField!
    @IBOutlet weak var regionTF: UITextField!
    @IBOutlet weak var carNumberView: UIView!
    @IBOutlet weak var zonePicker: UIPickerView!
    @IBOutlet weak var searchableSwitch: UISwitch!
    @IBOutlet weak var mainVehicleSwitch: UISwitch!
    @IBOutlet weak var deleteButtonCell: UITableViewCell!
    
    var vehicleHexColor: String?
    
    
    var id: Int?
    var firstlaunch = true
    var firstNumberChange = true
    
    var vehicle : Vehicle?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        zonePicker.dataSource = self
        zonePicker.delegate = self
        carNumberTF.delegate = self
        
        changeButton.addTarget(self, action: #selector(changeCarNumber), for: .touchUpInside)
        carNumberTF.addTarget(self, action: #selector(numberChange), for: .editingChanged)
        carNumberTF.addTarget(self, action: #selector(numberEndEditing), for: .editingDidEnd)
        
        self.hideKeyboardWhenTappedAround()
        
        //чтобы двигать view вместе с клавиатурой
        /*
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        */
        
        retrieveVehicle()
        carNumberTF.setNoBorder()
        regionTF.setNoBorder()
        carNumberView.stylyzateCarNumberView()
        
        tableView.tableFooterView = UIView()
    }
    
    //чтобы двигать view вместе с клавиатурой
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    //закрыть клавиатуру если нажать return
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        navigationController?.navigationBar.isHidden = false
    }
    
    // Это должно быть в ViewDidLoal, а не в ViewWillAppear
    func retrieveVehicle() {
        if vehicleHexColor != nil {
            self.carImage.backgroundColor = UIColor(hexString: (self.vehicleHexColor)!)
        }
        
        if id != nil {
            self.navigationItem.title = NSLocalizedString( "Редактировать информацию", comment: "" )
            deleteButtonCell.isHidden = false
            
            let vehicleApiService = VehicleApiService()
            vehicleApiService.get_vehicle(id: id!) { (result) -> () in
                self.vehicle = result
                
                print("result ", result)
                
                //разделяем номер авто на код региона и номер по пробелу
                let carNumberElements = self.vehicle!.number!.split(separator: " ")
                self.carNumberTF.text = String(carNumberElements[0])
                self.regionTF.text = String(carNumberElements[1])
                    // Это не должно быть тут, а в viewDidLoad
                self.vehicleHexColor = self.vehicle!.hex_color!
                self.carImage.backgroundColor = UIColor(hexString: (self.vehicleHexColor)!)

                if self.vehicle?.locale_code != nil && self.vehicle?.locale_code != nil {
                    //let index = carNumberZones.zones.firstIndex(of: self.vehicle!.locale_code!)
                    let index = 0
                    
                    if index != nil {
                        self.zonePicker.selectRow(index, inComponent: 0, animated: true)
                    }
                    
                }
                
                if self.vehicle?.searchable == false {
                    self.searchableSwitch.isOn = false
                }
                
                
            }
        } else {
            
            self.navigationItem.title = NSLocalizedString( "Добавить ТС", comment: "" )
            deleteButtonCell.isHidden = true
            vehicleHexColor = "#008060"

            
        }
        
        firstlaunch = false
        
    }
    
    
    
    @objc func changeCarNumber() {
        let vc = SelectColorViewController()
        // Получение цвета, который выбрал пользователь
        vc.sendColorBack = { [weak self] color in
            self?.vehicleHexColor = color
            self?.carImage.backgroundColor = UIColor(hexString: (self?.vehicleHexColor)!)
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func deleteCarNumber(_ sender: UIButton) {
        
        let alert = UIAlertController(title: NSLocalizedString("Вы уверены?", comment: "" ), message: "" , preferredStyle: UIAlertController.Style.alert)
        
        let alertAction = UIAlertAction(title: NSLocalizedString("Да", comment: "" ), style: UIAlertAction.Style.default){
            (UIAlertAction) -> Void in
            
            self.deleteAction()
        }
        
        let alertAction2 = UIAlertAction(title: NSLocalizedString( "Нет", comment: "" ), style: UIAlertAction.Style.default){
            (UIAlertAction) -> Void in
        }
        
        alert.addAction(alertAction)
        alert.addAction(alertAction2)
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func deleteAction(){
        LoadingIndicator.shared.startAnimation()
        let vehicleAPIService = VehicleApiService()

        vehicleAPIService.deleteVehicle (id: id) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func save(_ sender: UIButton) {
        LoadingIndicator.shared.startAnimation()
        let _carNumber = carNumberTF.text! + " " + regionTF.text!
        
        let locale_code = carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["systemName"]
        var is_main_transport = "1"
        if self.mainVehicleSwitch.isOn == false {
            is_main_transport = "0"
        }
        
        
        var searchable = "1"
        if self.searchableSwitch.isOn == false {
            searchable = "0"
        }

        let vehicleAPIService = VehicleApiService()
        
        if id == nil {
            vehicleAPIService.createVehicle (number: _carNumber, hex_color: vehicleHexColor, locale_code: locale_code, is_main_transport: is_main_transport, searchable: searchable) { (result) -> () in
                LoadingIndicator.shared.stopAnimation()
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)

            }
        } else {
            vehicleAPIService.updateVehicle (id: id, number: _carNumber, hex_color: vehicleHexColor , locale_code: locale_code, is_main_transport: is_main_transport, searchable: searchable){ (result) -> () in
                LoadingIndicator.shared.stopAnimation()
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }
    
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return carNumberZones.zones.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return carNumberZones.zones[row]["displayName"]
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 && indexPath.section == 0 {
            self.changeCarNumber()
        }
        
    }
    
    @objc func numberChange(){
        var _carNumber = carNumberTF.text
        
        if carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["displayName"] == "ru" {
            
            if _carNumber!.count == 6 && firstNumberChange {
                if !(_carNumber?.contains(" "))! {
                    //carNumberTF.resignFirstResponder()
                    regionTF.becomeFirstResponder()
                    firstNumberChange = false
                }
                
            }
        }
    }
    
    @objc func numberEndEditing(){
        print("numberEndEditing")
        var _carNumber = carNumberTF.text
        
        if carNumberZones.zones[zonePicker.selectedRow(inComponent: 0)]["displayName"] == "ru" && _carNumber!.count == 9 {
            
            if !(_carNumber?.contains(" "))! {
                var index = _carNumber!.index(_carNumber!.startIndex, offsetBy: 6)
                let substringFirst = String(_carNumber![..<index])
                print(substringFirst)
                
                index = _carNumber!.index(_carNumber!.endIndex, offsetBy: -3)
                let substringSecond = _carNumber![index...] // playground
                
                print(substringSecond)
                
                _carNumber = substringFirst + " " + substringSecond
                carNumberTF.text = _carNumber!
            }

            
            
        }
    }
    
    //только маленькие буквы и цифры в TF
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("маленькие")
        if string.isEmpty {
            return true
        }
        let regex = "[а-я, a-z, 0-9]{1,}"
        return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: string)
    }
    
    
}
