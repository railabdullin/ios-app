//
//  selectColorViewController.swift
//  beepme
//
//  Created by Macbook on 21/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import SnapKit

class SelectColorViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var collectionView : UICollectionView!
    //количество ячеек в строке
    let numberOfItemsPerRow = 4
    
   
    let colors = ["#ffffff",
    "#101419",
    "#ffff33",
    "#0067ce",
    "#ff0000",
    "#cc6633",
    "#edf5f6",
    "#0a7679",
    "#665739",
    "#336600",
    "#858585",
    "#008060",
    "#4a0206",
    "#9abeb7",
    "#afa38f",
    "#373737",
    "#574868",
    "#f2d48b",
    "#a4b5ca",
    "#3d4274",
    "#1c3937",
    "#abc4c5",
    "#b38c61",
    "#d5d3ae",
    "#7e0004",
    "#8e8a6f",
    "#6cc9ad",
    "#331159",
    "#bebebe",
    "#880f24",
    "#373b2c",
    "#8a6749",
    "#7e013d",
    "#6d7148",
    "#384f2f",
    "#d0aa9b",
    "#a6a600",
    "#009933",
    "#669966",
    "#ffc100",
    "#773c3c",
    "#b3faff",
    "#800040",
    "#ae0000",
    "#333333",
    "#796745",
    "#00974b",
    "#72b8b8",
    "#3a5e74",
    "#32011f",
    "#ffd700",
    "#fadadd"]
    
    var sendColorBack: ((String) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupCollectionView()
        setupConstraints()
    }
    
    private func setupCollectionView() {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    private func setupConstraints() {
        self.view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return colors.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        cell.backgroundColor = UIColor(hexString: colors[indexPath.row])
        
        cell.layer.masksToBounds = true
        cell.layer.cornerRadius = cell.frame.size.width / 2
        cell.clipsToBounds = true
        cell.layer.borderWidth = 1
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderColor = UIColor.gray.cgColor

        return cell
    }
    
    //количество ячеек в строке
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size - 5, height: size - 5)
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.sendColorBack?(self.colors[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
