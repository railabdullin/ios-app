//
//  CardRegisterWebViewController.swift
//  beepme
//
//  Created by Macbook on 04.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import UIKit
import WebKit

class CardRegisterWebViewController: UIViewController, WKNavigationDelegate {
    var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults.standard
        let uid = defaults.string(forKey: "uid")
        
        let url = URL(string: "https://numpass.com/api/card/register/start/?uid=\(uid!)")
        webView.load(URLRequest(url: url!))
    }
    
    override func loadView() {
        webView = WKWebView()
        webView.navigationDelegate = self
        view = webView
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
