//
//  authorizationAllViewController.swift
//  beepme
//
//  Created by Macbook on 20/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit

class authorizationAllViewController: UIViewController, UIPageViewControllerDelegate {
    
    @IBOutlet weak var pageController: UIPageControl!
    
    //СЛАЙДЫ
    @IBOutlet weak var phoneEnterView: UIView!
    @IBOutlet weak var codeEnterView: UIView!
    
    
    @IBOutlet weak var titleLabel: UILabel!
    
    //СЛАЙД ТЕЛЕФОН
    @IBOutlet weak var preficsLabel: UILabel!
    @IBOutlet weak var phoneTF: UITextField!
    
    //СЛАЙД КОД
    @IBOutlet weak var codeTF: UITextField!
    
    

    //var phone: String?
    //var code: String?
    var sessionKey: String?
    
    var hasActiveDialog = false
    var newMessegesCount = 0
    
    let authorizationService = APIAuthorizationService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageController.currentPage = 0
        self.pageController.numberOfPages = 2
        self.pageController.isHidden = false
        view.bringSubviewToFront(pageController)
        
        //ловим изменения втекстовом поле телефона
        phoneTF.addTarget(self, action: #selector(phoneChange), for: .editingChanged)
        codeTF.addTarget(self, action: #selector(codeChange), for: .editingChanged)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        phoneTF.becomeFirstResponder()

    }
    
    @objc func phoneChange(){
        print("телефон изменен")
        var _phone = phoneTF.text!
        
        if _phone.count == 13 && _phone.last == " " {
            _phone = String(_phone.dropLast())
        }
        
        if _phone.count == 12 && _phone.first! == "+" {
            print("удаляе первый")
            _phone = String(_phone.dropFirst(1))
        }
        print(_phone)
        
        if _phone.count == 11 {
            _phone = String(_phone.dropFirst(1))
            phoneTF.text = _phone
        }
        
        if _phone.count == 10 {
            
            _phone = preficsLabel.text! + _phone
            
            if self.phoneEnterView.isHidden == false {
                self.sendCode(phone: _phone)
            }

        }
    }
    
    @objc func codeChange(){
        let _code = codeTF.text
        if _code != nil {
            if _code!.count == 4 {
                //code = _code
                print("code0", _code!)
                self.authorization(code: _code!)
            }
        }
                
        
    }
    
    func sendCode(phone : String){
        LoadingIndicator.shared.startAnimation()
        
        authorizationService.send_code(phone: phone) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()
            //меняем экран на ввод кода
            self.phoneEnterView.isHidden = true
            self.codeEnterView.isHidden = false
            self.pageController.currentPage = 1
            
            //открываем клавиатуру
            self.phoneTF.resignFirstResponder()
            self.codeTF.becomeFirstResponder()
        }
       
    }
    
    func authorization(code: String){
        LoadingIndicator.shared.startAnimation()
        
        authorizationService.login(code: code ) { (result) -> () in
            LoadingIndicator.shared.stopAnimation()
            
            if result == "ok"{
                let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let mainController = storyboard.instantiateViewController(withIdentifier: "mainView")
                UIApplication.shared.keyWindow?.rootViewController = mainController
            } else {
                let alert = UIAlertController(title: "Не удалось авторизоваться", message: "Попробуйте заново ввести номер телефона и получить новый код", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Хорошо", style: .cancel, handler: nil))

                self.present(alert, animated: true)
            }
            
            
            
            //self.newMessegesCount = (decodedJSON?.new_messeges_count)!
            //self.hasActiveDialog = (decodedJSON?.has_last_connect)!
            
            //print("decodedJSON car_numbers dump")
            //dump(decodedJSON?.car_numbers)
            
            //if decodedJSON?.car_numbers != nil {
            //    print("записываем car_numbers")
            //    accountViewController.carNumbers = (decodedJSON?.car_numbers)!
            //}
            
            //print("decodedJSON blacklist dump")
            //dump(decodedJSON?.blacklist)
            
            //if decodedJSON?.blacklist != nil {
             //   print("записываем blacklist")
               /// blacklistViewController.blacklist = (decodedJSON?.blacklist)!
            //    print("vc2.blacklist dump")
            //    dump( blacklistViewController.blacklist)
            //}

            self.performSegue(withIdentifier: "segueToMainView", sender: nil)
            
        }
    }
    
    public func saveToFile(){
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0) as! NSString
        let path = documentsDirectory.appendingPathComponent("data.plist")
        let dict: NSMutableDictionary = ["XInitializerItem": "DoNotEverChangeMe"]
        
        //saving values
        dict.setObject(userData.phone, forKey: "phone" as NSCopying)
        dict.setObject(userData.sessionKey, forKey: "sessionKey" as NSCopying)
       
        dict.write(toFile: path, atomically: false)
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
        print("Saved GameData.plist file is --> \(resultDictionary?.description)")
        
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToMainView" {
            saveToFile()
            if let vc = segue.destination as? ViewController {
                vc.newMessegesCount = self.newMessegesCount
                vc.hasActiveDialog = self.hasActiveDialog
                
            }
        }
    }
    

}
