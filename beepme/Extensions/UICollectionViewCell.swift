//
//  UICollectionViewCell.swift
//  beepme
//
//  Created by Раиль Абдуллин on 05.10.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func heightForView(text:String, width:CGFloat) -> CGFloat{
       let font = UIFont(name: "Helvetica", size: 20.0)

       let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
       label.numberOfLines = 0
       label.lineBreakMode = NSLineBreakMode.byWordWrapping
       label.font = font
       label.text = text

       label.sizeToFit()
       return label.frame.height
   }
}
