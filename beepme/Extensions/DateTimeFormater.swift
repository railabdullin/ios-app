//
//  DateTimeFormater.swift
//  beepme
//
//  Created by Раиль Абдуллин on 22.09.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation

public class DateTimeFormatter {
    static func getDateTime(stringDate: String) -> String {
        var date: Date?
        let formatsString = ["yyyy-MM-dd'T'HH:mm:ss.SSSSZ", "yyyy-MM-dd'T'HH:mm:ssZ"]
        
        
        for format in formatsString {
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale.current
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSZ"
            date = dateFormatter.date(from: stringDate)
            
            if date != nil {
                break
            }
        }
        
        
        if date != nil {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateStyle = .medium
            dateFormatter2.timeStyle = .none
            dateFormatter2.locale = Locale.current
            dateFormatter2.dateFormat = "HH:mm dd.MM.yyyy "
            print(dateFormatter2.string(from: date!))
            
            return dateFormatter2.string(from: date!)
        } else {
            
            return ""
        }
    }
    
    static func getDate(stringDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSZ"
        
        //let date = dateFormatter.date(from: test )
    
        let date = dateFormatter.date(from: stringDate)
        
        if date != nil {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateStyle = .medium
            dateFormatter2.timeStyle = .none
            dateFormatter2.locale = Locale.current
            dateFormatter2.dateFormat = "dd.MM.yyyy"
            print(dateFormatter2.string(from: date!))
            
            return dateFormatter2.string(from: date!)
        } else {
            return ""
        }
    }
    
    static func getTime(stringDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.current // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSZ"
        
        //let date = dateFormatter.date(from: test )
    
        let date = dateFormatter.date(from: stringDate)
        
        if date != nil {
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateStyle = .medium
            dateFormatter2.timeStyle = .none
            dateFormatter2.locale = Locale.current
            dateFormatter2.dateFormat = "HH:mm"
            print(dateFormatter2.string(from: date!))
            
            return dateFormatter2.string(from: date!)
        } else {
            return ""
        }
    }
}
