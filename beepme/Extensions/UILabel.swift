//
//  UILabel.swift
//  beepme
//
//  Created by Раиль Абдуллин on 05.09.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func heightForView() -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = self.font
        label.text = self.text

        label.sizeToFit()
        return label.frame.height
    }
}
