//
//  jsonModels.swift
//  beepme
//
//  Created by Macbook on 20/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//
/*
import Foundation

     struct authorizationSendCode : Decodable{
        let success : Bool?
        let phone : String?
        let vercode: String?
        let code: Int?
        let description: String?
    }
    
     struct authorization: Decodable {
        let success: Bool?
        let sck : String?
        let description : String?
        let car_numbers: [carNumbers]?
        let last_connect_id : String?
        let blacklist: [blacklist]?
        let has_last_connect: Bool?
        let new_messeges_count : Int?
        let new_messege_sticker_id : Int?
        let new_messege_location_lattitude : String?
        let new_messege_location_longitude : String?
        let new_messege_car_number : String?
        let code: Int?
    }
    
     struct blacklist : Decodable{
        let id : String
        let blocked_user_id : String?
        let car_number : String?
        let car_color : String?
        let time: String?
    }
    
    struct carNumbers : Decodable{
        let id : String
        let number : String?
        let zone: String?
        let searchable: String?
        let alarmEnabled: String?
        let hex_color: String?
    }
    
    
     struct connects : Decodable{
        let success : Bool
        let id : String?
        let description : String?
        let code: Int?
        let connects: [connect]?
    }
    
     struct connect : Decodable{
        let id : String?
        let user_id : String?
        let author_car_number: String?
        let author_car_color: String?
        let sticker_id: String?
        let begin_time: String?
        let status: String?
    }
    
     struct messeges : Decodable{
        let success : Bool?
        let description : String?
        let stickers_for_sending: [Int]?
        let my_id : String?
        let location_lattitude : String?
        let location_longitude : String?
        let author_car_color : String?
        let author_car_number : String?
        let reciever_car_color : String?
        let reciever_car_number : String?
        let my_position : String?
        let code: Int?
        let messeges: [messege]?
    }
    
     struct messege : Decodable, Equatable{
        let id : String?
        let author_id: String?
        let reciever_id: String?
        let sticker_id: String?
        let status: String?
        let time: String?
        let blocked: String?
    }
    
     struct searchResult : Decodable{
        let success: Bool
        let description : String?
        let hex_color: String?
        let car_number: String?
        let author_id: String?
        var stickers_for_sending: [Int]?
        let code: Int?
    }
    
    

*/
