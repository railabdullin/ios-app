//
//  ButtonMessegeCell.swift
//  beepme
//
//  Created by Раиль Абдуллин on 07.10.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import UIKit

class ButtonMessegeCell: UITableViewCell {
    
    @IBOutlet weak var button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
