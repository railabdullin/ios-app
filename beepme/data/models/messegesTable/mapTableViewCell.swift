//
//  mapTableViewCell.swift
//  beepme
//
//  Created by Macbook on 22/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit
import MapKit

class mapTableViewCell: UITableViewCell {

    @IBOutlet weak var mapView: MKMapView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
