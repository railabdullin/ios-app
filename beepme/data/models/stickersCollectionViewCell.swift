
//
//  stickersCollectionViewCell.swift
//  beepme
//
//  Created by Macbook on 22/07/2019.
//  Copyright © 2019 Хобби. All rights reserved.
//

import UIKit

class stickersCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var title: UILabel!
}
