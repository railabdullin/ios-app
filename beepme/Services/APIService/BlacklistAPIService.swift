//
//  BlacklistAPIService.swift
//  beepme
//
//  Created by Macbook on 14.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import Alamofire

class BlacklistApiService {
    func get_list(page : Int? ,callback: @escaping (_ result: Array<BlacklistItem>?) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["Authorization" : "Bearer \(token!)"]
        
        request("\(serverURL)blacklist/" , headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)

            switch responseJSON.result {
            case .success( _):
                
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(GetBlacklistResponse.self, from: jsonData)
                    print(jsonData)
                    callback(jsonArray.results)

                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func delete(id : Int ,callback: @escaping (_ result: String?) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["Authorization" : "Bearer \(token!)"]
        
        request("\(serverURL)blacklist/\(id)" , method: .delete, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)

            switch responseJSON.result {
            case .success( _):
                callback("ok")

            case .failure(let error):
                print(error)
            }
        }
    }
    
    
}

struct BlacklistItem: Decodable {
    let id: Int?
    let author_profile: Int?
    let blocked_profile: Int?
    let blocked_vehicle: Vehicle?
    let time: String?
}

struct GetBlacklistResponse: Decodable {
    let count: Int?
    let next: String?
    let previous: String?
    let results: [BlacklistItem]?
}
