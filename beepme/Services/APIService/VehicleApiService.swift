//
//  VehicleService.swift
//  beepme
//
//  Created by Macbook on 01.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import Alamofire

class VehicleApiService{
    
    func search (locale_code: String?, carnumber : String?, callback: @escaping (_ result: Array<Vehicle>?) -> Void) {
        var _Vehicles : [Vehicle]?
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]

        
        var params = ["locale_code": locale_code!, "carnumber": carnumber!]
        
        
        
        request("\(serverURL)carnumbers/search/", parameters: params, headers: headers).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)

            switch responseJSON.result {
            case .success(let _):
                print(0)
                
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode([Vehicle].self, from: jsonData)
                    
                    callback(jsonArray)
                    
                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
    
    func get_my_vehicles(id : String?, page : Int? ,callback: @escaping (_ result: Array<Vehicle>?) -> Void) {
        var vehicles : [Vehicle]?
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["Authorization" : "Bearer \(token!)"]
        
        request("\(serverURL)person_ads" , headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)

            switch responseJSON.result {
            case .success( _):
                print(0)
                
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode([Vehicle].self, from: jsonData)
                    callback(jsonArray)

                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func get_vehicle(id: Int, callback: @escaping (_ result: Vehicle?) -> Void)  {
       
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["Authorization" : "Bearer \(token!)"]

        
        request("\(serverURL)carnumbers/\(id)", headers: headers ).responseJSON { responseJSON in

           guard let statusCode = responseJSON.response?.statusCode else { return }

           switch responseJSON.result {
           case .success( _):
               
               guard let jsonData = responseJSON.data else { return }
               
               do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(Vehicle.self, from: jsonData)
                    print("jsonArray", jsonArray)
                    callback(jsonArray)
                
                   
                    return
               } catch let error {
                   print(error)
               
                   return
               }
               

           case .failure(let error):
               print(error)
           }
       }
       
    }
    
    func createVehicle(number : String?, hex_color : String?, locale_code : String?, is_main_transport : String?, searchable : String? , callback: @escaping (_ result: Vehicle) -> Void)  {
        
        
        let jsonEncoder = JSONEncoder()
        var jsonData : Data?
    
        
       
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        var params = ["number" : number!, "hex_color" : hex_color!, "locale_code" : locale_code!, "is_main_transport" : is_main_transport!, "searchable" : searchable!] as [String : Any]
        print(params)
        
        var headers = ["pl" : "ios"]
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        if token != nil {
            headers["Authorization"] = "Bearer \(token!)"
        } else {
            print("Ошибка авторизации")
        }
        
        request("\(serverURL)carnumbers/create/", method: .post, parameters: params, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            
            if let data = responseJSON.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            switch responseJSON.result {
            case .success(let _):
                
                if statusCode == 200 {
                    do {
                        guard let jsonData = responseJSON.data else { return }
                        let decoder = JSONDecoder()
                        let jsonArray = try decoder.decode(Vehicle.self, from: jsonData)
                       
                        callback(jsonArray)
                    
                       
                        return
                    } catch let error {
                        print(error)
                   
                        return
                    }

                } else if statusCode == 406 {
                    print("Ошибка авторизации")
                }
               
                guard let jsonData = responseJSON.data else { return }
               
                
               

           case .failure(let error):
               print(error)
           }
       }
    }
    
    func updateVehicle(id: Int?, number : String?, hex_color : String?, locale_code : String?, is_main_transport : String?, searchable : String? , callback: @escaping (_ result: Vehicle) -> Void)  {
        
        
        let jsonEncoder = JSONEncoder()
        var jsonData : Data?
    
        
       
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        var params = ["number" : number!, "hex_color" : hex_color!, "locale_code" : locale_code!, "is_main_transport" : is_main_transport!, "searchable" : searchable!] as [String : Any]
        print(params)
        
        var headers = ["pl" : "ios"]
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        if token != nil {
            headers["Authorization"] = "Bearer \(token!)"
        } else {
            print("Ошибка авторизации")
        }
        
        request("\(serverURL)carnumbers/\(id!)", method: .put, parameters: params, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            
            if let data = responseJSON.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            switch responseJSON.result {
            case .success(let _):
                
                if statusCode == 200 {
                    do {
                        guard let jsonData = responseJSON.data else { return }
                        let decoder = JSONDecoder()
                        let jsonArray = try decoder.decode(Vehicle.self, from: jsonData)
                       
                        callback(jsonArray)
                    
                       
                        return
                    } catch let error {
                        print(error)
                   
                        return
                    }

                } else if statusCode == 406 {
                    print("Ошибка авторизации")
                }
               
                guard let jsonData = responseJSON.data else { return }
               
                
               

           case .failure(let error):
               print(error)
           }
       }
    }
    
    func deleteVehicle(id: Int? , callback: @escaping (_ result: String) -> Void)  {
        
        
        let jsonEncoder = JSONEncoder()
        var jsonData : Data?
    
        
       
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        
        var headers = ["pl" : "ios"]
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        if token != nil {
            headers["Authorization"] = "Bearer \(token!)"
        } else {
            print("Ошибка авторизации")
        }
        
        request("\(serverURL)carnumbers/\(id!)", method: .delete, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            
            if let data = responseJSON.data, let utf8Text = String(data: data, encoding: .utf8) {
                print("Data: \(utf8Text)")
            }
            
            switch responseJSON.result {
            case .success(let _):
                callback("ok")
                guard let jsonData = responseJSON.data else { return }
               

            case .failure(let error):
                print(error)
                if statusCode == 200 {
                    do {
                        guard let jsonData = responseJSON.data else { return }
                        let decoder = JSONDecoder()
                        let jsonArray = try decoder.decode(Vehicle.self, from: jsonData)
                       
                        callback("ok")
                    
                       
                        return
                    } catch let error {
                        print(error)
                   
                        return
                    }

                } else if statusCode == 406 {
                    print("Ошибка авторизации")
                }
           }
       }
    }
    
    func finishParking(sessionID: Int, callback: @escaping (_ result: [String : Any]?) -> Void)  {
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
               
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        var params = ["canceled": 1] as [String : Int]
        request("\(serverURL)parking/scanings/\(sessionID)/canceled_status", method: .put, parameters: params, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print(statusCode)
            
            if statusCode == 200 {
                let dict = [ "error" : 0] as [String : Any]
                callback(dict)
                
            }
           
            else if statusCode == 257 {
                let dict = [ "error" : 1] as [String : Any]
                callback(dict)
            }
       }
       
    }
    
    
    
    
    
    
}



struct Vehicle: Decodable {
    let id: Int?
    let number: String?
    let hex_color: String?
    let locale_code: String?
    let searchable: Bool?
    let alarmEnabled: Bool?
    let is_main_transport: Bool?
    
    
}
