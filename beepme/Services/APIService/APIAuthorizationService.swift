//
//  Authorization.swift
//  beepme
//
//  Created by Macbook on 01.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import Alamofire

class APIAuthorizationService {
    
    let defaults = UserDefaults.standard
    
    func login(code : String, callback: @escaping (_ result: String) -> Void) {
            
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        

        //let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path!)
            
        let serverURL = dict!.object(forKey: "serverURL")!
        let headers =  ["accept" : "application/json"]
        
        var phone = defaults.string(forKey: "phone")
        
        if phone == nil {
            print("Ошибка: phone пустой")
            callback("Ошибка: phone пустой")
            return
        }
        
        print("code1", code)

        var parameters : [String : String] = ["username": phone!, "password" : code]
            dump (parameters)
            
        request("\(serverURL)app-auth/" , method: .post, parameters: parameters, headers: headers ).responseJSON { responseJSON in

                guard let statusCode = responseJSON.response?.statusCode else { return }
                print("statusCode: ", statusCode)
                
                switch responseJSON.result{
                case .success( _):
                    
                    guard let jsonData = responseJSON.data else { return }
                    
                    do {
                        let decoder = JSONDecoder()
                        
                        if  responseJSON.response!.statusCode == 200 {
                            let jsonArray = try decoder.decode(authData.self, from: jsonData)
                            print(jsonArray)

                            self.defaults.set(jsonArray.token!, forKey: "token")
                            self.defaults.set(jsonArray.expiry!, forKey: "expiry")
                            self.defaults.synchronize ()
                            let token = self.defaults.string(forKey: "token")
                            
                            print("auth token \(token)")
                            
                            self.get_user_data () { (result) -> () in
                                let fcmToken = self.defaults.string(forKey: "fcmToken")
                                print("data fcmToken", fcmToken)
                                
                                if fcmToken !=  nil {
                                    self.updateFCMToken(FCMToken: fcmToken!)  { (result) -> () in
                                        callback("ok")
                                    }
                                } else {
                                    callback("ok")
                                }
                            }
                            return
                        } else if responseJSON.response!.statusCode == 400 {
                            guard let jsonData = responseJSON.data else { return }
                            
                            print("error 400")
                            let decoder = JSONDecoder()
                            let jsonArray = try decoder.decode(authError.self, from: jsonData)
                            print(jsonArray)
                            callback(jsonArray.non_field_errors![0])
                            
                            break
                        }
                        
                    } catch let error {
                        print(error)
                        return
                    }
                    
                case .failure(let error):
                    print(error)
                }
                    
            }
            
        
        }
        
    func get_user_data( callback: @escaping (_ result: getUserDataResponse?) -> Void)  {
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data

        //let path = Bundle.main.path(forResource: "properties", ofType: "plist")!
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = self.defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        print(token!)

        
        request("\(serverURL)my_user_data/" , headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            print("responseJSON \(responseJSON)")

            switch responseJSON.result {
            case .success( _):
                
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(getUserDataResponse.self, from: jsonData)
                    
                    self.defaults.set(jsonArray.id, forKey: "my_id")
                    self.defaults.set(jsonArray.blocked, forKey: "blocked")
                    self.defaults.set(jsonArray.new_messages_count, forKey: "newMessagesCount")
                    self.defaults.set(jsonArray.slug, forKey: "uid")
                    self.defaults.synchronize ()
                    
                    let vc = accountViewController()
                    vc.vehicles = jsonArray.vehicles
                    
                    if jsonArray.notification_device_token == nil {
                        let fcmToken = self.defaults.string(forKey: "fcmToken")
                        print("data fcmToken", fcmToken)
                        
                        if fcmToken !=  nil {
                            self.updateFCMToken(FCMToken: fcmToken!)  { (result) -> () in
                            }
                        } else {
                        }
                    }
                        
                        
                    
                    callback(jsonArray)

                    
                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func update_autobalance(value: Int, callback: @escaping (_ result: getUserDataResponse?) -> Void)  {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        let parameters = ["autobalance" : value]
        
        request("\(serverURL)my_user_data/", method: .put, parameters: parameters, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            print("responseJSON \(responseJSON)")

            switch responseJSON.result {
            case .success( _):
                
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(getUserDataResponse.self, from: jsonData)
                    
                    self.defaults.set(jsonArray.id, forKey: "my_id")
                    self.defaults.set(jsonArray.blocked, forKey: "blocked")
                    self.defaults.set(jsonArray.new_messages_count, forKey: "newMessagesCount")
                    self.defaults.set(jsonArray.autobalance, forKey: "autobalance")
                    self.defaults.set(jsonArray.cash, forKey: "cash")
                    self.defaults.set(jsonArray.slug, forKey: "uid")
                    self.defaults.synchronize ()
                    
                    let vc = accountViewController()
                    vc.vehicles = jsonArray.vehicles
                    
                    callback(jsonArray)

                    
                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func updateFCMToken(FCMToken: String, callback: @escaping (_ result: String?) -> Void)  {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        let parameters = ["notification_device_token" : FCMToken]
        print("parameters for update token ", parameters)
        
        request("\(serverURL)update_notification_token/", method: .put, parameters: parameters, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            print("responseJSON \(responseJSON)")

            switch responseJSON.result {
            case .success( _):
                print("success")
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(getUserDataResponse.self, from: jsonData)
                    callback("ok")

                    
                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
        
    func send_code(phone : String,  callback: @escaping (_ result: String) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let headers =  ["accept" : "application/json"]
        let parameters =  ["phone": phone]
        
        request("\(serverURL)send_code/" , method: .post, parameters: parameters, headers: headers ).responseJSON { responseJSON in
            print(responseJSON)
            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            if statusCode == 200 {
                let defaults = UserDefaults.standard
                defaults.set(phone, forKey: "phone")
                
                guard let jsonData = responseJSON.data else { return }
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(send_code_response.self, from: jsonData)
                    if jsonArray != nil {
                        if jsonArray.message != nil {
                            callback("ok")
                        }
                    }
                } catch let error {
                    print(error)
                    return
                }
                
                
            } else if statusCode == 400 {
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(send_code_response.self, from: jsonData)
                    
                    if jsonArray != nil {
                        if jsonArray.message != nil {
                            callback(jsonArray.message!)
                        }
                    }

                    return
                } catch let error {
                    print(error)
                
                    return
                }
            }
        }
    }
    
    func BankCardDelete( callback: @escaping (_ result: String) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        request("\(serverURL)card/remove/" , method: .post,  headers: headers ).responseString{ responseString in

            guard let statusCode = responseString.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            if statusCode == 200 {
                callback("ok")
                
                
            } else if statusCode == 400 {
                print("error 400")
            }
        }
    }
    
    
    
    func phoneUpdateSendCode(newPhone : String,  callback: @escaping (_ result: String) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        let parameters =  ["phone": newPhone]
        
        request("\(serverURL)send_code_for_phone_change/" , method: .post, parameters: parameters, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            if statusCode == 200 {
                
                guard let jsonData = responseJSON.data else { return }
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(send_code_response.self, from: jsonData)
                    if jsonArray != nil {
                        if jsonArray.message != nil {
                            callback("ok")
                        }
                    }
                } catch let error {
                    print(error)
                    return
                }
                
                
            } else if statusCode == 405 {
                callback("exist")

            }
        }
    }
    
    func phoneUpdateFinish(newPhone : String, code : String,  callback: @escaping (_ result: String) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        let parameters =  ["phone": newPhone, "code" : code]
        
        request("\(serverURL)update_phone/" , method: .put, parameters: parameters, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print("statusCode: ", statusCode)
            
            if statusCode == 200 {
                callback("ok")
                
            } else if statusCode == 400 {
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(send_code_response.self, from: jsonData)
                    
                    if jsonArray != nil {
                        if jsonArray.message != nil {
                            callback(jsonArray.message!)
                        }
                    }

                    return
                } catch let error {
                    print(error)
                
                    return
                }
            }
        }
    }
}

    struct authData: Decodable {
        let token: String?
        let expiry: String?
        let user: user?
        
    }

    struct send_code_response: Decodable {
        let message: String?
    }

    struct getUserDataResponse: Decodable {
        let id: Int?
        let blocked: Bool?
        let block_description: String?
        let cash: Int?
        let autobalance: Int?
        let has_active_connect: Bool?
        let new_messages_count: Int?
        let user: user?
        let vehicles: [Vehicle]?
        let credit_card_mask: String?
        let notification_device_token: String?
        let slug: String?
        let active_connect_id: Int?
    }
    
    struct user: Decodable {
        let id: Int?
        let username: String?
        let first_name: String?
        let last_name: String?
        let account_type: String?
        let name: String?
        let phone: String?
        let credit_card_mask: String?
        let slug: String?
    }

    struct rewiew: Decodable {
        let id: Int?
        let company: Int?
        let rate: Int?
        let description: String?
        let author: rewiewUser?
    }

    struct rewiewUser: Decodable {
        let id: Int?
        let first_name: String?
        let last_name: String?
        let profile_picture_1: String?
        
    }

    struct authError: Decodable {
        let non_field_errors: [String]?
        
    }
