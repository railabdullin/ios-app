//
//  ConnectsApiService.swift
//  beepme
//
//  Created by Macbook on 09.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

class ConnectsApiService {
    func get_connects(page : Int?, callback: @escaping (_ result: Array<Connect>?) -> Void) {
        
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
        
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        print(headers)
        
        request("\(serverURL)connects/" , headers: headers ).responseJSON { responseJSON in
            guard let statusCode = responseJSON.response?.statusCode else { return }

            switch responseJSON.result {
            case .success( _):
                guard let jsonData = responseJSON.data else { return }
                
                do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode([Connect].self, from: jsonData)
                    print(jsonArray)
                    callback(jsonArray)

                    return
                } catch let error {
                    print(error)
                
                    return
                }
                

            case .failure(let error):
                print(error)
            }
        }
    }
    
    func get_connect(id: Int, callback: @escaping (_ result: Connect?) -> Void)  {
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
               
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
       
        request("\(serverURL)connects/\(id)", headers: headers ).responseJSON { responseJSON in

           guard let statusCode = responseJSON.response?.statusCode else { return }
            print(responseJSON)
           switch responseJSON.result {
           case .success(let _):
               
               guard let jsonData = responseJSON.data else { return }
               
               do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(Connect.self, from: jsonData)
                   
                    callback(jsonArray)
                
                   
                    return
               } catch let error {
                   print(error)
               
                   return
               }
               

           case .failure(let error):
               print(error)
           }
       }
    }
    
    func createConnect(vehicleID: Int, stickerID: Int, location: CLLocation, callback: @escaping (_ result: String?) -> Void)  {
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
               
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        var params = ["sticker_id": stickerID, "carnumber_id": vehicleID, "longitude" : Double( location.coordinate.longitude), "lattitude" : Double( location.coordinate.latitude) ] as [String : Any]
       
        request("\(serverURL)connects/create/", method: .post, parameters: params, headers: headers ).responseJSON { responseJSON in

           guard let statusCode = responseJSON.response?.statusCode else { return }
            print(statusCode)
            
           if statusCode == 200 {
               guard let jsonData = responseJSON.data else { return }
               
               do {
                    let decoder = JSONDecoder()
                    let jsonArray = try decoder.decode(Connect.self, from: jsonData)
                   
                    callback("ok")
            
                    return
               } catch let error {
                   print(error)
               
                   return
               }
           } else if statusCode == 256 {
               callback("havenotcar")

           } else if statusCode == 257 {
               callback("blocked")

           } else if statusCode == 258 {
               callback("limit")

           }
       }
       
    }
    
    func sendSticker(connectID: Int, stickerID: Int, callback: @escaping (_ result: [String : Any]?) -> Void)  {
        let path: String? = Bundle.main.path(forResource: "property", ofType: "plist") //the path of the data
        let dict = NSDictionary(contentsOfFile: path!)
        let serverURL = dict!.object(forKey: "serverURL")!
        
        let defaults = UserDefaults.standard
        let token = defaults.string(forKey: "token")
               
        let headers =  ["accept" : "application/json", "Authorization" : "Bearer \(token!)"]
        
        var params = ["sticker_id": stickerID] as [String : Any]
       
        request("\(serverURL)connects/\(connectID)/send/", method: .post, parameters: params, headers: headers ).responseJSON { responseJSON in

            guard let statusCode = responseJSON.response?.statusCode else { return }
            print(statusCode)
            
            if statusCode == 200 {
                guard let jsonData = responseJSON.data else { return }
               
                do {
                    let decoder = JSONDecoder()
                        let jsonArray = try decoder.decode(Connect.self, from: jsonData)
                    
                    let dict = [ "connect" : jsonArray, "error" : 0] as [String : Any]
                        callback(dict)
                
                        return
                } catch let error {
                       print(error)
                   
                       return
                }
                
            }
           
            else if statusCode == 257 {
                let dict = [ "error" : 1] as [String : Any]
                callback(dict)
            }
       }
       
    }
    
}

struct Connect: Decodable {
    let id: Int?
    let author_carnumber: Vehicle?
    let reciever_carnumber: Vehicle?
    let parking_session: ParkingSession?
    let title: String?
    let type: String?
    let finished: Bool?
    let start_time: String?
    let messages: [Message]?
    let stickers_for_sending: [Int]?
    
    let location_longitude: Float?
    let location_lattitude: Float?
}

struct Message: Decodable {
    let id: Int?
    
    let recieved: Bool?
    
    let reciever: Int?
    let author: Int?
    
    let location_longitude: Float?
    let location_lattitude: Float?
    
    let sticker_id: Int?
    let text: String?
    let image_link: String?
    let time: String?
    
    let left_text: String?
    let right_text: String?
    
    let type: String?
}

struct ParkingSession: Decodable {
    let id: Int?
}
