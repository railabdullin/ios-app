//
//  PhoneMask.swift
//  beepme
//
//  Created by Macbook on 04.07.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation

class PhoneMaskHelper {
    func mask(phone: String) -> String {
        let last2 = phone.suffix(2)
        
        var newPhone = "+7 *** *** ** \(last2)"
        return newPhone
    }
}
