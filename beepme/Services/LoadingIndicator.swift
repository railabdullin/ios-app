//
//  LoadingIndicator.swift
//  beepme
//
//  Created by Khusnullin Denis on 21.09.2020.
//  Copyright © 2020 Хобби. All rights reserved.
//

import Foundation
import NVActivityIndicatorView


class LoadingIndicator {
    static let shared = LoadingIndicator()
    
    let loadingIndicator = NVActivityIndicatorView(frame: .zero, type: .lineSpinFadeLoader, color: #colorLiteral(red: 0.0862745098, green: 0.3647058824, blue: 0.4509803922, alpha: 1), padding: 0)
    let blackView = UIView()

    public func startAnimation() {
        if let window = UIApplication.shared.windows.filter({ $0.isKeyWindow}).first {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.1)
            window.addSubview(blackView)
            blackView.alpha = 1
            blackView.frame = window.frame
            loadingIndicator.startAnimating()
            
            blackView.addSubview(loadingIndicator)
            loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                loadingIndicator.centerXAnchor.constraint(equalTo: window.centerXAnchor),
                loadingIndicator.centerYAnchor.constraint(equalTo: window.centerYAnchor),
                loadingIndicator.heightAnchor.constraint(equalToConstant: 40),
                loadingIndicator.widthAnchor.constraint(equalToConstant: 40)
            ])
        }
    }
    
    public func stopAnimation() {
        blackView.alpha = 0
        blackView.removeFromSuperview()
        loadingIndicator.stopAnimating()
    }
}
